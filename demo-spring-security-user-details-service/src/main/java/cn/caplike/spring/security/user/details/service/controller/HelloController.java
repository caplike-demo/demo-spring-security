package cn.caplike.spring.security.user.details.service.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author LiKe
 * @version 1.0.0
 * @date 2020-04-15 12:58
 */
@Slf4j
@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello() {
        log.debug("authentication: {}", SecurityContextHolder.getContext().getAuthentication());
        return "Hello and congrats, you have successfully accessed inside!";
    }
}
