package cn.caplike.spring.security.user.details.service.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-04-17 10:38
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Role {

    private String id;
    private String name;

}
