package cn.caplike.spring.security.user.details.service.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-04-17 10:41
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MyUser {

    private int id;
    private String name;
    private String password;

}
