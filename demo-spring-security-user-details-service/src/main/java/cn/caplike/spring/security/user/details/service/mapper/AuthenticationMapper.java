package cn.caplike.spring.security.user.details.service.mapper;

import cn.caplike.spring.security.user.details.service.domain.entity.MyUser;
import cn.caplike.spring.security.user.details.service.domain.entity.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author LiKe
 * @version 1.0.0
 * @date 2020-04-16 18:06
 */
@Mapper
@Repository
public interface AuthenticationMapper {

    @Select("SELECT * FROM USER WHERE name = #{name}")
    MyUser loadUserByUsername(String name);

    @Select("SELECT role.name FROM ROLE as role WHERE role.id in (SELECT role_id FROM ROLE_USER as r_s JOIN USER as u ON r_s.user_id = u.id and u.id = #{id})")
    List<Role> findRoleByUserId(int id);
}
