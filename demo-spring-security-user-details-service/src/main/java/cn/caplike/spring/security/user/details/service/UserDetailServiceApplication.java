package cn.caplike.spring.security.user.details.service;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * demo-spring-security-user-detail-service 启动类<br>
 * <strong>spring-security 用户名/密码保存到数据库中</strong>
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-04-16 17:51
 */
@MapperScan(basePackages = "cn.caplike.spring.security.user.details.service.mapper")
@SpringBootApplication
public class UserDetailServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserDetailServiceApplication.class);
    }

}
