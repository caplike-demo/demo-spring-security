# About demo-spring-security-user-details-service

利用 UserDetailsService 从数据库获取用户账户名密码并完成校验. 关键点主要是配置自定义的 AuthenticationProvider.

[博客地址](https://blog.csdn.net/caplike/article/details/105895725)

# Reference

[从数据库中检索用户名和密码](https://www.cnblogs.com/young-youth/p/11665574.html)

# 更新日志

## 2020-4-21 13:27:08

​		The Very First Version.