/*
 Navicat Premium Data Transfer

 Source Server         : MySQL8#腾讯云-LiKe
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : 111.229.226.114:3306
 Source Schema         : demo_spring_security

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 30/06/2020 08:49:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for POPEDOM
-- ----------------------------
DROP TABLE IF EXISTS `POPEDOM`;
CREATE TABLE `POPEDOM`
(
    `id`          int(0)                                                 NOT NULL AUTO_INCREMENT COMMENT '主键 ID',
    `request_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '请求的 URL, 形如 /user/1 和 /admin/info',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '权限定义表.\r\n每一条权限代表着一个可访问的 Restful API.'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of POPEDOM
-- ----------------------------
INSERT INTO `POPEDOM`
VALUES (1, '/admin/info');
INSERT INTO `POPEDOM`
VALUES (2, '/user/*');

-- ----------------------------
-- Table structure for ROLE
-- ----------------------------
DROP TABLE IF EXISTS `ROLE`;
CREATE TABLE `ROLE`
(
    `id`   int(0)                                                 NOT NULL AUTO_INCREMENT,
    `name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ROLE
-- ----------------------------
INSERT INTO `ROLE`
VALUES (1, 'ROLE_ADMIN');
INSERT INTO `ROLE`
VALUES (2, 'ROLE_USER');

-- ----------------------------
-- Table structure for ROLE_POPEDOM
-- ----------------------------
DROP TABLE IF EXISTS `ROLE_POPEDOM`;
CREATE TABLE `ROLE_POPEDOM`
(
    `role_id`    int(0) NOT NULL COMMENT 'ROLE 的 ID',
    `popedom_id` int(0) NOT NULL COMMENT 'POPEDOM 的 ID',
    PRIMARY KEY (`role_id`, `popedom_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '角色-权限关联表.\r\n一个角色可以有多个权限, 多个角色可以有相同权限, 角色和权限是本张表的联合主键.'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ROLE_POPEDOM
-- ----------------------------
INSERT INTO `ROLE_POPEDOM`
VALUES (1, 1);
INSERT INTO `ROLE_POPEDOM`
VALUES (2, 2);

-- ----------------------------
-- Table structure for ROLE_USER
-- ----------------------------
DROP TABLE IF EXISTS `ROLE_USER`;
CREATE TABLE `ROLE_USER`
(
    `user_id` int(0) NULL DEFAULT NULL,
    `role_id` int(0) NULL DEFAULT NULL
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ROLE_USER
-- ----------------------------
INSERT INTO `ROLE_USER`
VALUES (1, 1);
INSERT INTO `ROLE_USER`
VALUES (2, 1);

-- ----------------------------
-- Table structure for USER
-- ----------------------------
DROP TABLE IF EXISTS `USER`;
CREATE TABLE `USER`
(
    `id`       int(0)                                                  NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `name`     varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '名字',
    `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
    `role`     varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 7
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of USER
-- ----------------------------
INSERT INTO `USER`
VALUES (1, 'root', '$2a$10$yjx08FV8gARdM7YinjFp7u.aD3dyDYgBwzWl84qYFLFLNhn3R1Vs2', 'USER');
INSERT INTO `USER`
VALUES (2, 'like', '$2a$10$yjx08FV8gARdM7YinjFp7u.aD3dyDYgBwzWl84qYFLFLNhn3R1Vs2', 'USER');

SET FOREIGN_KEY_CHECKS = 1;
