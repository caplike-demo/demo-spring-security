package c.c.d.s.s.o.ts.as.domain.user.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户职权
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-22 09:57
 */
@Data
@NoArgsConstructor
public class UserAuthority {

    /**
     * 用户职权 ID
     */
    private String id;

    /**
     * 用户职权名称
     */
    private String name;

    /**
     * 用户职权描述
     */
    private String description;

}
