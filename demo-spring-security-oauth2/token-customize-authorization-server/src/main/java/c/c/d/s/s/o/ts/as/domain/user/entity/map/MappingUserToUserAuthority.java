package c.c.d.s.s.o.ts.as.domain.user.entity.map;

import c.c.d.s.s.o.ts.as.domain.user.entity.User;
import c.c.d.s.s.o.ts.as.domain.user.entity.UserAuthority;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户 {@link User} - 用户职权 {@link UserAuthority} 的映射对象
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-22 10:01
 */
@Data
@NoArgsConstructor
public class MappingUserToUserAuthority {

    /**
     * {@link User} 的 ID
     */
    private String userId;

    /**
     * {@link UserAuthority} 的 ID
     */
    private String userAuthorityId;

}
