package c.c.d.s.s.o.ts.as;

import com.alibaba.fastjson.parser.ParserConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 启动类
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-07-07 15:54
 */
@SpringBootApplication
@MapperScan(basePackages = "c.c.d.s.s.o.ts.as.mapper")
public class TokenCustomizeAuthorizationServer {

    public static void main(String[] args) {
        SpringApplication.run(TokenCustomizeAuthorizationServer.class);

        ParserConfig.getGlobalInstance().addAccept("c.c.d.s.s.o.ts.as.domain.");
    }

    // ~ Bean
    // -----------------------------------------------------------------------------------------------------------------

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}


