/**
 * Spring Security OAuth 2.0 CLIENT Relevant
 *
 * @author LiKe
 * @date 2020-06-15 10:11
 */
package c.c.d.s.s.o.ts.as.configuration.support.client;