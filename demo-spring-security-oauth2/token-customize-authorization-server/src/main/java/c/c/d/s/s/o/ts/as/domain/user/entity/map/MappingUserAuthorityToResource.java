package c.c.d.s.s.o.ts.as.domain.user.entity.map;

import c.c.d.s.s.o.ts.as.domain.Resource;
import c.c.d.s.s.o.ts.as.domain.user.entity.UserAuthority;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户职权 {@link UserAuthority} - 资源 {@link Resource} 的映射对象
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-22 10:26
 */
@Data
@NoArgsConstructor
public class MappingUserAuthorityToResource {

    /**
     * {@link UserAuthority} 的 ID
     */
    private String userAuthorityId;

    /**
     * {@link Resource} 的 ID
     */
    private String resourceId;

}
