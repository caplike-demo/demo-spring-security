package c.c.d.s.s.o.ts.as.domain.client.entity.map;

import c.c.d.s.s.o.ts.as.domain.client.entity.Client;
import c.c.d.s.s.o.ts.as.domain.client.entity.ResourceServer;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * {@link Client} to {@link ResourceServer} 映射实体
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-17 11:22
 */
@Data
@NoArgsConstructor
public class MappingClientToResourceServer {

    /**
     * {@link Client#getId()}
     */
    private String clientId;

    /**
     * {@link ResourceServer#getId()}
     */
    private String resourceServerId;

}
