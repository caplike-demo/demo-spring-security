package c.c.d.s.s.o.ts.as.domain.client.dto;

import c.c.d.s.s.o.ts.as.domain.client.entity.Client;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * {@link Client} Dto
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-15 14:54
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ClientDTO extends Client {

    /**
     * 客户端可访问的资源 Id
     */
    private Set<String> resourceIds;

    /**
     * 客户端职权
     */
    private Set<String> authorities;

}
