package c.c.d.s.s.o.p.resource.server.controller;

import com.alibaba.fastjson.JSON;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ResourceController
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-13 21:11
 */
@RestController
@RequestMapping("/resource")
public class ResourceController {

    @GetMapping("/access")
    public String access() {
        return JSON.toJSONString(SecurityContextHolder.getContext());
    }

}
