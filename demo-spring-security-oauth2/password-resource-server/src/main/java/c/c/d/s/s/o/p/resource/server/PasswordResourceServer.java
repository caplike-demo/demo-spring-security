package c.c.d.s.s.o.p.resource.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Security OAuth 2.0 Password Grant Resource Server Application
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-11 16:44
 */
@SpringBootApplication
public class PasswordResourceServer {

    public static void main(String[] args) {
        SpringApplication.run(PasswordResourceServer.class);
    }

}
