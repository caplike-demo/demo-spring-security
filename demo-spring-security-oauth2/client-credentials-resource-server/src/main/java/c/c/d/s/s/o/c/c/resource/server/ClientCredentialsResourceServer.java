package c.c.d.s.s.o.c.c.resource.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Security OAuth 2.0 - Client Credentials Grant Resource Server 启动类
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-14 21:11
 */
@SpringBootApplication
public class ClientCredentialsResourceServer {

    public static void main(String[] args) {
        SpringApplication.run(ClientCredentialsResourceServer.class);
    }

}
