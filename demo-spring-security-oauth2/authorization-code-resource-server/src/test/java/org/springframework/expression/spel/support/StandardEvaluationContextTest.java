package org.springframework.expression.spel.support;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

/**
 * StandardEvaluationContext 测试类<br>
 * <b>About SpEL</b>: The Spring Expression Language (SpEL) 是一种强大的表达式语言, 允许使用者在运行时查询和操作对象图.<br>
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-07 16:51
 */
@Slf4j
public class StandardEvaluationContextTest {

    /**
     * A simple test of {@link ExpressionParser}
     */
    @Test
    public void testTheUseOfExpressionParser() {
        ExpressionParser expressionParser = new SpelExpressionParser();

        // 调用 getBytes().length
        final Integer bytesLength = expressionParser.parseExpression("'Hello World'.bytes.length").getValue(Integer.class);
        System.out.println("'Hello World' bytes length is: " + bytesLength);
    }

    /**
     * A simple test for {@link StandardEvaluationContext}
     */
    @Test
    public void testStandardEvaluationContext() {
        ExpressionParser expressionParser = new SpelExpressionParser();

        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setRootObject(new Root(true));
        context.setVariable("name", new Name("caplike"));

        // 调用 rootObject 的方法
        log.debug("{}", expressionParser.parseExpression("isRoot('caplike')").getValue(context, Boolean.class));
        // 调用 name 所对应对象的 is 方法: false
        log.debug("{}", expressionParser.parseExpression("#name.is('like')").getValue(context, Boolean.class));
        // 调用 name 所对应对象的 is 方法: true
        log.debug("{}", expressionParser.parseExpression("#name.is('caplike')").getValue(context, Boolean.class));
    }

    private static class Root {
        private final boolean root;

        public Root(Boolean root) {
            this.root = root;
        }

        public boolean isRoot(String state) {
            log.debug("{} :: Root#isRoot() called ...", state);
            return root;
        }
    }

    private static class Name {
        private final String specifiedName;

        public Name(String specifiedName) {
            this.specifiedName = specifiedName;
        }

        public boolean is(String name) {
            return specifiedName.equals(name);
        }
    }

}
