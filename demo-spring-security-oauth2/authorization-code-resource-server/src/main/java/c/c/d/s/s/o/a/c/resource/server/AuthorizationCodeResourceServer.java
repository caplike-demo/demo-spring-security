package c.c.d.s.s.o.a.c.resource.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Security OAuth 2.0 授权码模式-资源服务器 启动类
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-02 14:09
 */
@SpringBootApplication
public class AuthorizationCodeResourceServer {

    public static void main(String[] args) {
        SpringApplication.run(AuthorizationCodeResourceServer.class);
    }
}
