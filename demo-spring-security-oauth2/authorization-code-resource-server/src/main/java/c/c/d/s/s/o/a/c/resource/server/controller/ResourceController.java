package c.c.d.s.s.o.a.c.resource.server.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ResourceController
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-03 14:24
 */
@RestController
@RequestMapping("/resource-server")
public class ResourceController {

    @GetMapping("/access")
    public String access() {
        return "authorization-code-resource-server: you're in!";
    }

}
