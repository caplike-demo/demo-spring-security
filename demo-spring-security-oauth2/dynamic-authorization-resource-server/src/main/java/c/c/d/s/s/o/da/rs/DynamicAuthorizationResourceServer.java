package c.c.d.s.s.o.da.rs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 动态权限 - 资源服务器启动类
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-07-24 17:09
 */
@SpringBootApplication
public class DynamicAuthorizationResourceServer {

    public static void main(String[] args) {
        SpringApplication.run(DynamicAuthorizationResourceServer.class);
    }

    // ~ Bean
    // -----------------------------------------------------------------------------------------------------------------

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
