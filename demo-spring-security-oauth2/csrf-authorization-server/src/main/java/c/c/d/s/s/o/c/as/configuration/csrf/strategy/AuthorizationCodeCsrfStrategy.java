package c.c.d.s.s.o.c.as.configuration.csrf.strategy;

import c.c.d.s.s.o.c.as.context.GrantType;
import org.apache.commons.collections4.MapUtils;

import java.util.Map;
import java.util.Objects;

/**
 * Description: CSRF 防护机制: 授权码模式策略
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-08-10 14:17
 */
public class AuthorizationCodeCsrfStrategy extends AbstractCsrfStrategy {

    private static final String FORM_GRANT_TYPE = "grant_type";

    @Override
    public GrantType getGrantType() {
        return GrantType.AUTHORIZATION_CODE;
    }

    @Override
    public boolean supports(Map<String, ?> parameters) {
        return (getGrantType().getCode()).equals(MapUtils.getString(parameters, FORM_GRANT_TYPE)) && Objects.nonNull(MapUtils.getString(parameters, "code"));
    }

}
