package c.c.d.s.s.o.c.as.context;

/**
 * Description: 授权类型枚举
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-08-10 13:45
 */
public enum GrantType {

    /**
     * 授权码模式
     */
    AUTHORIZATION_CODE("authorization_code"),

    /**
     * 隐式模式
     */
    IMPLICIT("implicit"),

    /**
     * 密码模式
     */
    PASSWORD("password"),

    /**
     * 客户端模式
     */
    CLIENT_CREDENTIALS("client_credentials"),

    /**
     * 刷新令牌
     */
    REFRESH_TOKEN("refresh_token");

    private final String code;

    GrantType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
