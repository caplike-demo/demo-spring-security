package c.c.d.s.s.o.c.as.domain.client.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 客户端职权
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-15 15:44
 */
@Data
@NoArgsConstructor
public class ClientAuthority {

    /**
     * 客户端职权 ID (主键)
     */
    private String id;

    /**
     * 职权名称
     */
    private String name;

}
