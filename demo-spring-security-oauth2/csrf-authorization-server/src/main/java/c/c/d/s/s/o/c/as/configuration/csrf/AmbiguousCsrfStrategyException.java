package c.c.d.s.s.o.c.as.configuration.csrf;

/**
 * Description: 对于当前请求, 符合 CSRF 保护机制的策略有 0 个或者 大于1 个时, 抛出该异常
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-08-10 16:03
 */
public class AmbiguousCsrfStrategyException extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "Ambiguous csrf protecting strategy: %d";

    public AmbiguousCsrfStrategyException(int eligibleStrategyNum) {
        super(String.format(MESSAGE_TEMPLATE, eligibleStrategyNum));
    }
}
