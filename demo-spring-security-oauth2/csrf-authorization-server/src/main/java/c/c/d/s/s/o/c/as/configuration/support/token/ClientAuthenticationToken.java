package c.c.d.s.s.o.c.as.configuration.support.token;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.oauth2.provider.ClientDetails;

/**
 * Description: 客户端认证令牌
 *
 * @author LiKe
 * @date 2020-08-07 09:57:16
 */
public class ClientAuthenticationToken extends AbstractAuthenticationToken {

    private final Object principal;

    private final Object credentials;

    public ClientAuthenticationToken(ClientDetails clientDetails) {
        super(clientDetails.getAuthorities());
        this.principal = clientDetails.getClientId();
        this.credentials = clientDetails.getClientSecret();
        super.setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return credentials;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }
}