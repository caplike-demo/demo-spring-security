package c.c.d.s.s.o.c.as.configuration.support.token.granter;

import c.c.d.s.s.o.c.as.context.OAuth2AuthenticationHolder;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

/**
 * Description: 自定义的 {@link ClientCredentialsTokenGranter}
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-08-12 19:50
 */
public class CustomClientCredentialsTokenGranter extends ClientCredentialsTokenGranter {

    public CustomClientCredentialsTokenGranter(AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService, OAuth2RequestFactory requestFactory) {
        super(tokenServices, clientDetailsService, requestFactory);
    }

    /**
     * @see CustomAuthorizationCodeTokenGranter#getOAuth2Authentication(ClientDetails, TokenRequest)
     */
    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
        final OAuth2Authentication oAuth2Authentication = super.getOAuth2Authentication(client, tokenRequest);

        // ~ 将认证对象与线程绑定
        OAuth2AuthenticationHolder.set(oAuth2Authentication);
        return oAuth2Authentication;
    }
}
