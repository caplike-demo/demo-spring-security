package c.c.d.s.s.o.c.as.configuration.csrf.strategy;

import c.c.d.s.s.o.c.as.context.GrantType;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections4.MapUtils;

import java.util.Map;

/**
 * Description: CSRF 防护机制: 隐式模式策略
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-08-10 16:37
 */
public class ImplicitCsrfStrategy extends AbstractCsrfStrategy {

    private static final String TOKEN = "token";

    private static final String KEY_AUTHORIZATION_REQUEST = "authorizationRequest";

    private static final String KEY_RESPONSE_TYPES = "responseTypes";

    @Override
    public GrantType getGrantType() {
        return GrantType.IMPLICIT;
    }

    @Override
    public boolean supports(Map<String, ?> parameters) {
        final JSONObject authorizationRequest = (JSONObject) MapUtils.getObject(parameters, KEY_AUTHORIZATION_REQUEST);
        final JSONArray responseTypes = authorizationRequest.getObject(KEY_RESPONSE_TYPES, JSONArray.class);
        return responseTypes.contains(TOKEN);
    }

}
