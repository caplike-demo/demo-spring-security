package c.c.d.s.s.o.c.as.configuration.csrf.strategy;

import c.c.d.s.s.o.c.as.context.GrantType;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * Description: CSRF 防护机制: 密码模式策略
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-08-12 19:33
 */
public class ResourceOwnerPasswordCsrfStrategy extends AbstractCsrfStrategy {

    private static final String FORM_GRANT_TYPE = "grant_type";

    @Override
    public GrantType getGrantType() {
        return GrantType.PASSWORD;
    }

    @Override
    public boolean supports(Map<String, ?> parameters) {
        return StringUtils.equals(MapUtils.getString(parameters, FORM_GRANT_TYPE), getGrantType().getCode());
    }

}
