package c.c.d.s.s.o.c.as.configuration.csrf.strategy;

import c.c.d.s.s.o.c.as.context.GrantType;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * Description: 刷新令牌的 CSRF 策略实现
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-08-13 16:31
 */
public class RefreshTokenCsrfStrategy extends AbstractCsrfStrategy {

    private static final String FORM_GRANT_TYPE = "grant_type";

    @Override
    public GrantType getGrantType() {
        return GrantType.REFRESH_TOKEN;
    }

    @Override
    public boolean supports(Map<String, ?> parameters) {
        return StringUtils.equals(MapUtils.getString(parameters, FORM_GRANT_TYPE), getGrantType().getCode());
    }
}
