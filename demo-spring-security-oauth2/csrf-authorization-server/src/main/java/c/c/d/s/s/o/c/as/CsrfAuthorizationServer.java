package c.c.d.s.s.o.c.as;

import com.alibaba.fastjson.parser.ParserConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Description: 授权服务器
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-08-05 12:53
 */
@SpringBootApplication
@MapperScan(basePackages = "c.c.d.s.s.o.c.as.repository")
@ServletComponentScan(basePackages = "c.c.d.s.s.o.c.as.configuration.csrf")
public class CsrfAuthorizationServer {

    public static void main(String[] args) {
        SpringApplication.run(CsrfAuthorizationServer.class);

        ParserConfig.getGlobalInstance().addAccept("c.c.d.s.s.o.c.as.domain.");
    }

    // ~ Bean
    // -----------------------------------------------------------------------------------------------------------------

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
