# About Client Credentials Grant

> 博文地址: 

​		应用无前端, 只有后端的场景. 整个过程用户没有介入 (注意, 虽然密码模式也不需要前端, 但是是有用户信息介入的, 客户端授权模式是 4 种模式中唯一一种完全不需要用户介入的模式), 是第三方应用 (Client) 本身的行为. 直接与授权服务器交互, 用配置中的客户端信息去申请 `access-token`. 客户端有自己的 client_id 和 client_secret 对应用户的 username 和 password, 而客户端也有自己的 authorities (当采用这个模式时, 对应的权限也就是客户端自己的 authorities)

```
http://localhost:18709/client-credentials-authorization-server/oauth/token?grant_type=client_credentials&client_id=client-a&client_secret=client-a-p
```



**客户端使用自己的标识换token，客户端使用token访问资源**

**不需要用户介入, 主要用于第一方应用**



![Client credentials - 客户端](../doc/diagram/diagram-oauth 2.0-client-credentials-grant.png)



# Reference

[Spring Security OAuth2 Demo —— 客户端模式（ClientCredentials）](https://www.cnblogs.com/hellxz/p/12041588.html)