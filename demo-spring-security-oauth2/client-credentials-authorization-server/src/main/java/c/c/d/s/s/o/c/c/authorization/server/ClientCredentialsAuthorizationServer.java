package c.c.d.s.s.o.c.c.authorization.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Security OAuth 2.0 - Client Credentials Grant Authorization Server 启动类
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-14 21:09
 */
@SpringBootApplication
public class ClientCredentialsAuthorizationServer {

    public static void main(String[] args) {
        SpringApplication.run(ClientCredentialsAuthorizationServer.class);
    }

}
