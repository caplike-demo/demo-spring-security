package c.c.d.s.s.o.c.c.authorization.server.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Security 配置
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-14 21:27
 */
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http.formLogin().disable();

        http.httpBasic().disable();

        http.authorizeRequests().anyRequest().authenticated();
        // @formatter:on
    }
}
