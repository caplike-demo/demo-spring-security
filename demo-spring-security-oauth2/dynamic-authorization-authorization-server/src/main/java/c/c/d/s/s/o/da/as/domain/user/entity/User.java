package c.c.d.s.s.o.da.as.domain.user.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * User 实体对象
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-22 09:55
 */
@Data
@NoArgsConstructor
public class User {

    /**
     * 用户 ID
     */
    private String id;

    /**
     * 用户密码 (加密过的)
     */
    private String password;

    /**
     * 用户名
     */
    private String username;

}
