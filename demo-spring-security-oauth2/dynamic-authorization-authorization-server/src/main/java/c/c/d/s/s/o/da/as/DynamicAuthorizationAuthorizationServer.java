package c.c.d.s.s.o.da.as;

import com.alibaba.fastjson.parser.ParserConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 动态权限 - 授权服务器启动类
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-07-24 17:07
 */
@SpringBootApplication
@MapperScan(basePackages = "c.c.d.s.s.o.da.as.repository")
public class DynamicAuthorizationAuthorizationServer {

    public static void main(String[] args) {
        SpringApplication.run(DynamicAuthorizationAuthorizationServer.class);

        ParserConfig.getGlobalInstance().addAccept("c.c.d.s.s.o.da.as.domain.");
    }

    // ~ Bean
    // -----------------------------------------------------------------------------------------------------------------

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
