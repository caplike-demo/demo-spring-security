package c.c.d.s.s.o.da.as.domain.user.dto;

import c.c.d.s.s.o.da.as.domain.user.entity.User;
import c.c.d.s.s.o.da.as.domain.user.entity.UserAuthority;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * User DTO
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-22 10:44
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class UserDTO extends User {

    /**
     * 当前用户对应的职权 {@link UserAuthority}s
     */
    private Set<String> authorities;

}
