package c.c.d.s.s.o.da.as.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 资源实体
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-15 17:08
 */
@Data
@NoArgsConstructor
public class Resource {

    /**
     * 主键 (UUID)
     */
    private String id;

    /**
     * 标识资源的 ID
     */
    private String resourceId;

    /**
     * 资源端点, 形如 /user/1
     */
    private String endpoint;

}
