package c.c.d.s.s.o.da.as.configuration.support;

import c.c.d.s.s.o.da.as.configuration.support.response.ResponseWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义的 {@link AccessDeniedHandler}
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-15 11:43
 */
@Slf4j
@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException {
        log.debug("Custom AccessDeniedHandler triggered with exception: {}.", accessDeniedException.getClass().getCanonicalName());

        ResponseWrapper.unauthorizedResponse(response, accessDeniedException.getMessage());
    }

}
