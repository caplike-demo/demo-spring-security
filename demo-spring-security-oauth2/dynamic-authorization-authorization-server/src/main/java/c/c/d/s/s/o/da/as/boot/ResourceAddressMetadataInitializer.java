package c.c.d.s.s.o.da.as.boot;

import c.c.d.s.s.o.da.as.domain.client.dto.ClientAccessScopeResourceAddressMapping;
import c.c.d.s.s.o.da.as.domain.client.dto.ClientAuthorityResourceAddressMapping;
import c.c.d.s.s.o.da.as.domain.user.dto.UserAuthorityResourceAddressMapping;
import c.c.d.s.s.o.da.as.repository.client.ClientAccessScopeMapper;
import c.c.d.s.s.o.da.as.repository.client.ClientAuthorityMapper;
import c.c.d.s.s.o.da.as.repository.user.UserAuthorityMapper;
import cn.caplike.data.redis.service.spring.boot.starter.RedisKey;
import cn.caplike.data.redis.service.spring.boot.starter.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Description: 资源地址元数据初始化<br>
 * Details: 授权服务器启动的时候, 从数据源里加载访问控制香瓜你的元数据, 并放入缓存. 供资源服务器调用
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-08-03 14:33
 */
@Order(1)
@Slf4j
@Component
public class ResourceAddressMetadataInitializer implements ApplicationRunner {

    /**
     * metadata.resource-address 缓存前缀
     */
    private static final String CACHE_PREFIX_METADATA_RESOURCE_ADDRESS = "metadata.resource-address";

    private RedisService redisService;

    // ~ ResourceAddress Mappers
    // -----------------------------------------------------------------------------------------------------------------

    private ClientAccessScopeMapper clientAccessScopeMapper;

    private ClientAuthorityMapper clientAuthorityMapper;

    private UserAuthorityMapper userAuthorityMapper;

    // =================================================================================================================

    @Override
    public void run(ApplicationArguments args) {
        log.info("Resource address metadata initializing ...");

        final RedisService.Hash redisServiceOpsForHash = redisService.hash();

        redisServiceOpsForHash.putAll(
                RedisKey.builder().prefix(CACHE_PREFIX_METADATA_RESOURCE_ADDRESS).suffix(ClientAccessScopeResourceAddressMapping.CACHE_SUFFIX).build(),
                clientAccessScopeMapper.composeClientAccessScopeResourceAddressMapping().stream().collect(Collectors.toMap(
                        ClientAccessScopeResourceAddressMapping::getClientAccessScopeName,
                        ClientAccessScopeResourceAddressMapping::getResourceAddress
                ))
        );
        log.debug("==============================================================================");
        log.debug("|            Metadata: ClientAccessScope - ResourceAddress Cached            |");
        log.debug("==============================================================================");

        redisServiceOpsForHash.putAll(
                RedisKey.builder().prefix(CACHE_PREFIX_METADATA_RESOURCE_ADDRESS).suffix(ClientAuthorityResourceAddressMapping.CACHE_PREFIX).build(),
                clientAuthorityMapper.composeClientAuthorityResourceAddressMapping().stream().collect(Collectors.toMap(
                        ClientAuthorityResourceAddressMapping::getClientAuthorityName,
                        ClientAuthorityResourceAddressMapping::getResourceAddress
                ))
        );
        log.debug("==============================================================================");
        log.debug("|             Metadata: ClientAuthority - ResourceAddress Cached             |");
        log.debug("==============================================================================");

        redisServiceOpsForHash.putAll(
                RedisKey.builder().prefix(CACHE_PREFIX_METADATA_RESOURCE_ADDRESS).suffix(UserAuthorityResourceAddressMapping.CACHE_PREFIX).build(),
                userAuthorityMapper.composeUserAuthorityResourceAddressMapping().stream().collect(Collectors.toMap(
                        UserAuthorityResourceAddressMapping::getUserAuthorityName,
                        UserAuthorityResourceAddressMapping::getResourceAddress
                ))
        );
        log.debug("==============================================================================");
        log.debug("|              Metadata: UserAuthority - ResourceAddress Cached              |");
        log.debug("==============================================================================");

        log.info("Resource address metadata initialized.");
    }

    // ~ Autowired
    // -----------------------------------------------------------------------------------------------------------------

    @Autowired
    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }

    @Autowired
    public void setClientAccessScopeMapper(ClientAccessScopeMapper clientAccessScopeMapper) {
        this.clientAccessScopeMapper = clientAccessScopeMapper;
    }

    @Autowired
    public void setClientAuthorityMapper(ClientAuthorityMapper clientAuthorityMapper) {
        this.clientAuthorityMapper = clientAuthorityMapper;
    }

    @Autowired
    public void setUserAuthorityMapper(UserAuthorityMapper userAuthorityMapper) {
        this.userAuthorityMapper = userAuthorityMapper;
    }
}
