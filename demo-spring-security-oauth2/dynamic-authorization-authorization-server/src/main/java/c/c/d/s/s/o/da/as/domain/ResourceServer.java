package c.c.d.s.s.o.da.as.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 资源实体
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-15 14:47
 */
@Data
@NoArgsConstructor
public class ResourceServer {

    /**
     * 资源服务器 ID (主键)
     */
    private String id;

    /**
     * 资源密钥 (加密后)
     */
    private String resourceSecret;

    /**
     * 资源服务器的描述信息
     */
    private String description;

}
