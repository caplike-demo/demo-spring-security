package c.c.d.s.s.o.da.as.configuration.support.token;

import c.c.d.s.s.o.da.as.configuration.support.response.SecurityResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.util.Map;

/**
 * 自定义的 {@link JwtAccessTokenConverter}
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-07-22 10:19
 */
public class CustomJwtAccessTokenConverter extends JwtAccessTokenConverter {

    /**
     * Description: 重写以返回统一的格式
     *
     * @return java.util.Map<java.lang.String, java.lang.String>
     * @author LiKe
     * @date 2020-07-22 10:23:04
     * @see JwtAccessTokenConverter#getKey()
     */
    @Override
    public Map<String, String> getKey() {
        return SecurityResponse.Builder.of().httpStatus(HttpStatus.OK).message(HttpStatus.OK.getReasonPhrase())
                .data(super.getKey())
                .build().toMap();
    }
}
