package c.c.d.s.s.o.ts.rs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Token 自定义资源服务器
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-07-21 19:25
 */
@SpringBootApplication
public class TokenCustomizeRemoteTokenServicesResourceServer {

    public static void main(String[] args) {
        SpringApplication.run(TokenCustomizeRemoteTokenServicesResourceServer.class);
    }

}
