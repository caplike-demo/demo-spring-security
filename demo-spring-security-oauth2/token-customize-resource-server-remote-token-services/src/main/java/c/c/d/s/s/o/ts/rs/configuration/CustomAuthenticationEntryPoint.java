package c.c.d.s.s.o.ts.rs.configuration;

import c.c.d.s.s.o.ts.rs.configuration.support.response.ResponseWrapper;
import c.c.d.s.s.o.ts.rs.configuration.support.response.SecurityResponse;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义的 {@link AuthenticationEntryPoint}
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-07-23 15:29
 */
@Slf4j
@Component
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        log.debug("Custom AuthenticationEntryPoint triggered with exception: {}.", authException.getClass().getCanonicalName());

        // 原始异常信息
        final String authExceptionMessage = authException.getMessage();

        try {
            final SecurityResponse securityResponse = JSON.parseObject(authExceptionMessage, SecurityResponse.class);
            ResponseWrapper.wrapResponse(response, securityResponse);
        } catch (JSONException ignored) {
            ResponseWrapper.forbiddenResponse(response, authExceptionMessage);
        }
    }

}
