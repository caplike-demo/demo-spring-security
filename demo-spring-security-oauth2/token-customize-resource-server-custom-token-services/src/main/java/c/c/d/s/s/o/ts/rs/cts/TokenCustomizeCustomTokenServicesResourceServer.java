package c.c.d.s.s.o.ts.rs.cts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: 资源服务器启动类
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-07-23 19:41
 */
@SpringBootApplication
public class TokenCustomizeCustomTokenServicesResourceServer {

    public static void main(String[] args) {
        SpringApplication.run(TokenCustomizeCustomTokenServicesResourceServer.class);
    }

}
