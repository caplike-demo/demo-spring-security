# About implicit grant

> Spring Security OAuth 2.0 的隐式模式
>
> 博文地址: https://blog.csdn.net/caplike/article/details/107023974

response_type=token, 表示直接返回 token. 整个过程没有后端介入, token 可以直接在前端, 所以很不安全.

隐式授权模式要求: 用户登录并对第三方应用进行授权, 直接返回访问 access-token, 通过token访问资源.

相比授权码模式, 它少了一次授权码的颁发与客户端使用授权码换取  access-token 的过程.

```
http://localhost:18777/implicit-authorization-server/oauth/authorize?response_type=token&client_id=client-a&scope=access_resource
```

返回形如: 

```
http://localhost:9001/callback#access_token=edc84920-5588-44f5-80d1-8d9cf79d3d14&token_type=bearer&expires_in=119
```

直接利用 access_token 访问资源服务器的资源

![](../doc/diagram/diagram-oauth 2.0-implicit-grant.png)



**适用场景**

- 用户参与: 使用隐式授权需要与用户交互, 用户对授权服务器进行登录与授权
- client_secret: 访问授权时, **不需要**带第三方应用的 client_secret, 前提是资源服务校验 access_token 使用的客户端信息与第三方应用不同, 且配置了 client_secret.
- 必须要有前端



**整体流程**

1. 客户端让用户登陆授权服务器换 access_token
   1. 客户端 (浏览器或者 SPA) 将 client_id + 授权模式标识 (response_type) + 回调地址 (redirect_uri) 拼成 url 访问授权服务器的授权端点;
   2. 授权服务器跳转到用户登陆界面, 用户登陆
   3. 用户授权
   4. 授权服务器访问回调地址返回 access_token 给客户端
2. 客户端使用 access_token 访问资源
   1. 客户端在请求头中添加 access_token, 访问资源服务器;
   2. 资源服务器收到请求, 先调用校验 access_token 的方法 (可以是远程调用授权服务器的校验端点, 也可以直接访问授权存储器手动校对)
   3. 资源服务器校验成功, 返回资源



# Reference

- [Spring Security OAuth2 Demo —— 隐式授权模式（Implicit）](https://www.cnblogs.com/hellxz/p/oauth2_impilit_pattern.html)