package c.c.d.s.s.o.i.authorization.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Security OAuth 2.0 隐式 - 授权服务器 - 启动类
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-10 11:03
 */
@SpringBootApplication
public class ImplicitAuthorizationServer {

    public static void main(String[] args) {
        SpringApplication.run(ImplicitAuthorizationServer.class);
    }

}
