package c.c.d.s.s.o.i.resource.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Security OAuth 2.0 隐式 - 资源服务器 - 启动类
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-10 11:27
 */
@SpringBootApplication
public class ImplicitResourceServer {

    public static void main(String[] args) {
        SpringApplication.run(ImplicitResourceServer.class);
    }

}
