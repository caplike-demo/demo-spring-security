package c.c.d.s.s.o.c.rs.configuration;

import c.c.d.s.s.o.c.rs.configuration.support.response.ResponseWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义的 {@link AuthenticationEntryPoint}
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-07-23 15:29
 */
@Slf4j
@Component
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        log.debug("Custom AuthenticationEntryPoint triggered with exception: {}.", authException.getClass().getCanonicalName());
        ResponseWrapper.forbiddenResponse(response, authException.getMessage());
    }

}
