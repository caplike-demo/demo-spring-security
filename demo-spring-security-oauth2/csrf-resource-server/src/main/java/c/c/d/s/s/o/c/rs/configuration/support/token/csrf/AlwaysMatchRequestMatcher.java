package c.c.d.s.s.o.c.rs.configuration.support.token.csrf;

import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;

/**
 * Description: 总是匹配的 {@link RequestMatcher}
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-08-15 15:28
 */
public class AlwaysMatchRequestMatcher implements RequestMatcher {

    @Override
    public boolean matches(HttpServletRequest request) {
        return true;
    }
}
