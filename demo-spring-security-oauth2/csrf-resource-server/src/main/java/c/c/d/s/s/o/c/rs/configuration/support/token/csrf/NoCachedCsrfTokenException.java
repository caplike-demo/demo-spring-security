package c.c.d.s.s.o.c.rs.configuration.support.token.csrf;

/**
 * Description: 没有缓存的 CSRF-TOKEN 的异常
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-08-15 14:56
 */
public class NoCachedCsrfTokenException extends RuntimeException {

    public NoCachedCsrfTokenException(String message) {
        super(message);
    }
}
