package c.c.d.s.s.o.c.rs.controller;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ResourceController
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-13 21:11
 */
@Slf4j
@RestController
@RequestMapping("/resource")
public class ResourceController {

    @GetMapping("/access")
    public String access() {
        final String securityContext = JSON.toJSONString(SecurityContextHolder.getContext());
        log.debug("resource-server accessed with security context: ");
        log.debug(securityContext);
        return securityContext;
    }

}
