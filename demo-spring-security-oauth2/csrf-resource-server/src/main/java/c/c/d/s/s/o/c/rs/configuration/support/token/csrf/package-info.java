/**
 * Description: 资源服务器 CSRF 策略包. TODO 后期可以抽取成 Starter 复用.
 *
 * @author LiKe
 * @date 2020-08-09 18:32
 */
package c.c.d.s.s.o.c.rs.configuration.support.token.csrf;