package c.c.d.s.s.o.c.rs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 资源服务器
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-08-05 13:21
 */
@SpringBootApplication
public class CsrfResourceServer {

    public static void main(String[] args) {
        SpringApplication.run(CsrfResourceServer.class);
    }

    // ~ Bean
    // -----------------------------------------------------------------------------------------------------------------

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
