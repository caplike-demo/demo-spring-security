package c.c.d.s.s.o.csd.as;

import com.alibaba.fastjson.parser.ParserConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * ClientDetailsService 授权服务器
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-15 09:31
 */
@SpringBootApplication
@MapperScan(basePackages = "c.c.d.s.s.o.csd.as.mapper")
public class ClientDetailsServiceAuthorizationServer {

    public static void main(String[] args) {
        SpringApplication.run(ClientDetailsServiceAuthorizationServer.class);

        ParserConfig.getGlobalInstance().addAccept("c.c.d.s.s.o.csd.as.domain.");
    }

    // ~ Bean
    // -----------------------------------------------------------------------------------------------------------------

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
