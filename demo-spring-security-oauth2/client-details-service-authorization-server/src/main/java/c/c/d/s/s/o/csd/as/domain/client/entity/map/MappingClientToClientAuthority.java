package c.c.d.s.s.o.csd.as.domain.client.entity.map;

import c.c.d.s.s.o.csd.as.domain.client.entity.Client;
import c.c.d.s.s.o.csd.as.domain.client.entity.ClientAuthority;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * {@link Client} to {@link ClientAuthority} 映射实体
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-17 11:29
 */
@Data
@NoArgsConstructor
public class MappingClientToClientAuthority {

    /**
     * {@link Client#getId()}
     */
    private String clientId;

    /**
     * {@link ClientAuthority#getId()}
     */
    private String clientAuthorityId;

}
