package c.c.d.s.s.o.csd.as.domain.user.entity.map;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户职权 {@link c.c.d.s.s.o.csd.as.domain.user.entity.UserAuthority} - 资源 {@link c.c.d.s.s.o.csd.as.domain.Resource} 的映射对象
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-22 10:26
 */
@Data
@NoArgsConstructor
public class MappingUserAuthorityToResource {

    /**
     * {@link c.c.d.s.s.o.csd.as.domain.user.entity.UserAuthority} 的 ID
     */
    private String userAuthorityId;

    /**
     * {@link c.c.d.s.s.o.csd.as.domain.Resource} 的 ID
     */
    private String resourceId;

}
