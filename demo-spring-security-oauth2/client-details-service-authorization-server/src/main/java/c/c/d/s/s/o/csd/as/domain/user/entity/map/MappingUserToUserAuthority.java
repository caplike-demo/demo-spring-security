package c.c.d.s.s.o.csd.as.domain.user.entity.map;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户 {@link c.c.d.s.s.o.csd.as.domain.user.entity.User} - 用户职权 {@link c.c.d.s.s.o.csd.as.domain.user.entity.UserAuthority} 的映射对象
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-22 10:01
 */
@Data
@NoArgsConstructor
public class MappingUserToUserAuthority {

    /**
     * {@link c.c.d.s.s.o.csd.as.domain.user.entity.User} 的 ID
     */
    private String userId;

    /**
     * {@link c.c.d.s.s.o.csd.as.domain.user.entity.UserAuthority} 的 ID
     */
    private String userAuthorityId;

}
