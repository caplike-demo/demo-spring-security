package c.c.d.s.s.o.csd.as.configuration.support.client;

import c.c.d.s.s.o.csd.as.domain.client.dto.ClientDTO;
import c.c.d.s.s.o.csd.as.mapper.client.ClientMapper;
import cn.caplike.data.redis.service.spring.boot.starter.RedisKey;
import cn.caplike.data.redis.service.spring.boot.starter.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * 自定义的 {@link ClientDetailsService}
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-15 12:41
 */
@Slf4j
@Service
public class CustomClientDetailsService implements ClientDetailsService {

    private ClientMapper clientMapper;

    private RedisService redisService;

    /**
     * Description: 从数据库中获取已经注册过的客户端信息<br>
     * Details: 该方法会在整个认证过程中被多次调用, 所以应该缓存. 缓存过期时间在 access_token 有效期的基础上加一个时间 buffer
     *
     * @param clientId 客户端 ID
     * @see ClientDetailsService#loadClientByClientId(String)
     */
    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        log.debug("About to produce ClientDetails with client-id: {}", clientId);

        final RedisKey cacheKey = RedisKey.builder().prefix("auth").suffix(clientId).build();

        // 先从缓存中获取 ClientDto
        ClientDTO clientDto = redisService.getValue(cacheKey, ClientDTO.class);
        // 如果缓存中没有, 从数据库查询并置入缓存
        if (Objects.isNull(clientDto)) {
            clientDto = clientMapper.getClient(clientId);

            if (Objects.isNull(clientDto)) {
                throw new ClientRegistrationException(String.format("客户端 %s 尚未注册!", clientId));
            }

            // Buffer: 10s
            redisService.setValue(cacheKey, clientDto, clientDto.getAccessTokenValidity() + 10);
        }

        return new CustomClientDetails(clientDto);
    }

    // ~ Autowired
    // -----------------------------------------------------------------------------------------------------------------

    @Autowired
    public void setClientMapper(ClientMapper clientMapper) {
        this.clientMapper = clientMapper;
    }

    @Autowired
    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }
}
