package org.springframework.security.crypto.bcrypt;

import org.junit.Before;
import org.junit.Test;

/**
 * A Tester for {@link BCryptPasswordEncoder}
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-04 12:04
 */
public class BCryptPasswordEncoderTest {

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Before
    public void before() {
        bCryptPasswordEncoder = new BCryptPasswordEncoder();
    }

    @Test
    public void testEncoder() {
        bCryptPasswordEncoder = new BCryptPasswordEncoder();
        System.out.println(bCryptPasswordEncoder.encode("client-a-p"));
    }

    @Test
    public void testMatch() {
        System.out.println(bCryptPasswordEncoder.matches("root", bCryptPasswordEncoder.encode("root")));
    }
}
