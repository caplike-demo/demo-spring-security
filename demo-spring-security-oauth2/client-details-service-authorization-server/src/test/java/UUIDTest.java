import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.UUID;

/**
 * A Tester for {@link UUID}
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-17 08:55
 */
public class UUIDTest {

    @Test
    public void randomUUIDTest() {
        System.out.println(StringUtils.replace(UUID.randomUUID().toString(), "-", StringUtils.EMPTY));
    }

}
