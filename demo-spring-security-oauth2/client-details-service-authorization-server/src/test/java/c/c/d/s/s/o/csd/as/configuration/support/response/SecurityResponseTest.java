package c.c.d.s.s.o.csd.as.configuration.support.response;

import org.junit.Test;
import org.springframework.http.HttpStatus;

public class SecurityResponseTest {

    @Test
    public void serialize() {
        System.out.println(SecurityResponse.Builder.of().httpStatus(HttpStatus.INSUFFICIENT_STORAGE).message("Some message.").build().toString());
    }

}