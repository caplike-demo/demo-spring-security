package com.fasterxml.jackson.databind;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;

/**
 * @author LiKe
 * @version 1.0.0
 * @date 2020-07-01 11:39
 */
public class ObjectMapperTest {

    @Test
    public void testWritingValueAsString() throws IOException {
        final HashMap<Object, Object> map = new HashMap<>(5);
        map.put("name", "LiKe");
        map.put("level", "5");

        System.out.println(new ObjectMapper().writeValueAsString(map));
    }

}
