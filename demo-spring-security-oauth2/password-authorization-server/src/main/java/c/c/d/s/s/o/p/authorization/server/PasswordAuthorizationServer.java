package c.c.d.s.s.o.p.authorization.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Security OAuth 2.0 Password Grant Authorization Server Application
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-11 16:42
 */
@SpringBootApplication
public class PasswordAuthorizationServer {

    public static void main(String[] args) {
        SpringApplication.run(PasswordAuthorizationServer.class);
    }

}
