# About Resource Owner Password Grant

> Spring Security OAuth 2.0 Password Grant
>
> ref: org.springframework.security.oauth2.provider.password.ResourceOwnerPasswordTokenGranter
>
> 博文地址: https://blog.csdn.net/caplike/article/details/107036739

​		密码模式有一个前提就是你高度信任第三方应用. 举个不恰当的例子: 如果我要在 mine.com 上接入微信登录, 我使用了密码模式, 那你就要在 mine.com 这个网站去输入微信的用户名密码, 这肯定是不靠谱的, 所以密码模式需要你非常信任第三方应用.

☼ 自己本身有一套用户体系, 在认证时需要带上自己的用户名和密码, 以及客户端的 client_id, client_secret. 此时, access-token 包含的权限是用户本身的权限, 而不是客户端的权限

☼ **可以没有前端介入** (有别于 Authorization Code Grant 和 Implicit Grant)

```
http://localhost:18907/password-authorization-server/oauth/token?grant_type=password&client_id=client-a&client_secret=client-a-p&username=caplike&password=caplike-p&scope=read_scope
```

授权服务器返回数据:

```json
{
    "access_token": "aa5a459e-4da6-41a6-bf67-6b8e50c7663b",
    "token_type": "bearer",
    "expires_in": 119,
    "scope": "read_scope"
}
```



![](../doc/diagram/diagram-oauth 2.0-resource-owner-password-grant.png)





**整体流程**

1. 让用户填写表单提交到授权服务器，表单中包含用户的用户名、密码、客户端的id和密钥的加密串

2. *授权服务器先解析并校验客户端信息，然后校验用户信息，*完全通过返回access_token，否则默认都是401 http状态码，提示未授权无法访问



# Reference

[Spring Security OAuth2 Demo —— 密码模式（Password）](https://www.cnblogs.com/hellxz/p/12041495.html)