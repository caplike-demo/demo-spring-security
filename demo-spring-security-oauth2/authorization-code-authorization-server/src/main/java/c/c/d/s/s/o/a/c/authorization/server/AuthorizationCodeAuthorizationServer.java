package c.c.d.s.s.o.a.c.authorization.server;

import com.alibaba.fastjson.parser.ParserConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Security OAuth 2.0 授权码模式-授权服务器 启动类
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-02 13:59
 */
@SpringBootApplication
@MapperScan(basePackages = "c.c.d.s.s.o.a.c.authorization.server.mapper")
public class AuthorizationCodeAuthorizationServer {

    public static void main(String[] args) {
        SpringApplication.run(AuthorizationCodeAuthorizationServer.class);

        ParserConfig.getGlobalInstance().addAccept("c.c.d.s.s.o.a.c.authorization.server.domain.");
    }
}
