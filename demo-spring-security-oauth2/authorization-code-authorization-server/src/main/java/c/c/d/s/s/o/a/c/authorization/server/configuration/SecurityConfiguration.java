package c.c.d.s.s.o.a.c.authorization.server.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Spring Security 配置<br>
 * 授权码模式需要用户登陆授权.
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-02 13:06
 */
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private UserDetailsService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .formLogin()

                .and()
                .authorizeRequests().anyRequest().authenticated()

        // ~ 不能禁用 Session, 否则在用户登陆后无法重新跳转回 /oauth/authorize
        // -----------------------------------------------------------------------------------------------------
        // .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        // ~ exception handling
        // -----------------------------------------------------------------------------------------------------
        // .and()
        // .exceptionHandling()
        // ~ 被 ExceptionTranslationFilter 调用用于异常处理
        // .authenticationEntryPoint((request, response, authException) -> {
        //     final RequestCache requestCache = new HttpSessionRequestCache();
        //     // http://localhost:18910/authorization-server/oauth/authorize?client_id=client-id-caplike&client_secret=client-secret-caplike&response_type=code&scope=all
        //     final String redirectUrl = requestCache.getRequest(request, response).getRedirectUrl();
        //     final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

        //     System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        //     System.out.println(request.getRequestURI());

        //     if (!StringUtils.equals(request.getRequestURI(), "/authorization-server/login")) {
        //         redirectStrategy.sendRedirect(request, response, "/login");
        //         // ResponseUtils.redirectResponse(response, "/authorization-server/login");
        //     }
        // })
        // .accessDeniedHandler((request, response, accessDeniedException) -> ResponseUtils.forbiddenResponse(response, accessDeniedException.getMessage()))
        ;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    // ~ autowired
    // -----------------------------------------------------------------------------------------------------------------

    @Autowired
    public void setUserDetailsService(@Qualifier("customUserDetailsService") UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }
}
