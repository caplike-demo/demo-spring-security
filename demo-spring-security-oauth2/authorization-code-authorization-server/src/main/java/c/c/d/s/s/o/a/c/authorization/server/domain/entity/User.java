package c.c.d.s.s.o.a.c.authorization.server.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * User Domain
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-02 17:12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {

    private String name;

    private String password;

    private String role;
}
