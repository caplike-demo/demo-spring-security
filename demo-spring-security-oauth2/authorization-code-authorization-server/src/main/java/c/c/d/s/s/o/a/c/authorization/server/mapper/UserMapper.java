package c.c.d.s.s.o.a.c.authorization.server.mapper;

import c.c.d.s.s.o.a.c.authorization.server.domain.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * UserMapper
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-02 17:07
 */
@Repository
@Mapper
public interface UserMapper {

    @Select("SELECT USERNAME AS NAME, PASSWORD, 'USER' AS ROLE FROM USER WHERE USERNAME = #{username}")
    User get(String username);

}
