package org.springframework.security.crypto.bcrypt;

import org.junit.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author LiKe
 * @version 1.0.0
 * @date 2020-06-03 18:07
 */
public class BCryptPasswordEncoderTest {

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Test
    public void encode() {
        System.out.println(passwordEncoder.encode("client-secret-caplike"));
    }
}
