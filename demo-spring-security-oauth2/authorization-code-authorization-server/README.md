# About authorization-code grant

> Spring Security OAuth 2.0 的授权码模式
>
> 这种方式是最常用的流程, 安全性也最高, 它适用于那些有后端的 Web 应用. 授权码通过前端传送, 令牌则是储存在后端, 而且所有与资源服务器的通信都在后端完成. 这样的前后端分离, 可以避免令牌泄漏.
>
> ref: org.springframework.security.oauth2.provider.code.AuthorizationCodeTokenGranter
>
> 博文地址: https://blog.csdn.net/caplike/article/details/107017131

​		获取授权码的流程一般是由客户端使用自己的 client-id 和 client-secret 加上 response_type=code 拼接 url  让浏览器跳转完成的. 用户的登陆与授权过程都需要在浏览器中完成.

​		在授权码模式中有授权服务器和资源服务器, 授权服务器用于颁发令牌, 拿着令牌就可以去资源服务器获取资源, 这两个服务器可以分开也可以合并.

​		一般情况下我们认为授权码模式是四种模式中最安全的一种模式, 因为这种模式我们的 access_token 不用经过浏览器或者移动端 App, 是直接从我们的后台发送到授权服务器上, 这样就很大程度减少了 access_token 泄漏的风险



![Authorization Code - 授权码模式时序](../doc/diagram/diagram-oauth 2.0-authorization-code-grant.png)



## 简要流程

☀ **第三方应用**首先有一个按钮, 按钮请求己方应用的如下地址:  http://localhost:18910/authorization-server/oauth/authorize?client_id=client-id-caplike&client_secret=client-secret-caplike&response_type=code&scope=access

- response_type=code, 表示授权码模式, 要求返回授权码, 将来用这个授权码获取 access-token.
- client_id 表示客户端 id, 也就是应用的 id. 如果想让我们的系统介入微信登陆, 肯定得去微信官方平台注册, 去填入我自己应用的基本信息等等. 弄完之后, 微信会给我一个 APPID, 也就是我这里的 client_id. 所以, 从这里可以看出, 授权服务器在校验的时候会做两件事: 1. 校验客户端的身份; 2. 校验用户身份.
- redirect_uri 表示用户在登陆成功/失败后, 跳转的地址 (成功登陆微信后, 跳转到我们系统的哪个页面). 跳转的时候还会携带上一个授权码参数.
- scope 表示授权范围, 即我们网站拿到令牌后能干嘛 (一般来说就是获取用户非敏感的基本信息)

☀ **用户** 点击按钮后, 提示登录, 默认会被己方应用重定向到 /login 端点. 登录后跳转到授权页面: 浏览器 URL 保持在: http://localhost:18910/authorization-server/oauth/authorize?client_id=client-id-caplike&client_secret=client-secret-caplike&response_type=code&scope=access

☀ **用户** 同意后, 授权服务器将授权码返回给客户端. 会调用在授权服务器中配置的回调地址: http://localhost:18910/authorization-server/access?code=pDipXD

☀ **第三方应用** 再用授权码访问如下地址获取 access-token: http://localhost:18910/authorization-server/oauth/token?code=pDipXD&grant_type=authorization_code&client_id=client-id-caplike&client_secret=client-secret-caplike&scope=access

POST 请求结果:

```json
{
    "additionalInformation": {},
    "expiration": 1591362205089,
    "expired": false,
    "expiresIn": 43199,
    "refreshToken": {
        "expiration": 1593911005087,
        "value": "e682d8da-a900-4c2f-817e-d7b937dff693"
    },
    "scope": [
        "access"
    ],
    "tokenType": "bearer",
    "value": "b6c6361c-dbc4-4c37-9b9d-1e86e1ed3180"
}
```

☀ **第三方应用** 即可用这个 access_token 访问己方范围内的资源: http://localhost:18711/authorization-code-resource-server/resource-server/access, `Authorization: Bearer <access_token>`



## 客户端 (Client) 的职责

1. 去授权服务器注册, 获取唯一标识客户端的 client_id 和 client_secret;
2. 提供到授权服务器 /oauth/authorize 的 URL 让用户登陆授权, 获取授权码;
3. 不收集用户凭证, 用授权码去授权服务器获取 access-token;
4. 用 access-token 访问资源服务器



## 授权服务器 (Authorization Server) 的职责

1. 颁发 access-token
2. 确认用户是否授权客户端的行为
3. 认证用户 /authorize
4. 认证客户端 (通过 authorization-code)



## 资源服务器 (Resource Server) 的职责

1. 解析 access-token
2. 访问控制: scope, audience, user account info (id, roles etc.), client info (id, roles etc.)
3. 如果 access-token 不正确或是不存在等情况, 返回 403



# 总结

此模式要求授权服务器与用户直接交互, 在此过程中, 第三方应用是无法获取到用户输入的密码等信息的. 所以这个模式也是 OAuth 2.0 最安全的一个.



# Reference

[Spring Security OAuth2 Demo —— 授权码模式 (Authorization Code)](https://www.cnblogs.com/hellxz/p/oauth2_oauthcode_pattern.html)