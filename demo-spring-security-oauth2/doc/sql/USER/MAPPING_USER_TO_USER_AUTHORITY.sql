/*
 Navicat Premium Data Transfer

 Source Server         : MySQL8#腾讯云-LiKe
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : 111.229.226.114:3306
 Source Schema         : demo_spring_security_oauth2

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 30/07/2020 10:48:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for MAPPING_USER_TO_USER_AUTHORITY
-- ----------------------------
DROP TABLE IF EXISTS `MAPPING_USER_TO_USER_AUTHORITY`;
CREATE TABLE `MAPPING_USER_TO_USER_AUTHORITY`
(
    `USER_ID`           varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户 ID',
    `USER_AUTHORITY_ID` varbinary(32)                                         NOT NULL COMMENT '用户职权 ID',
    PRIMARY KEY (`USER_ID`, `USER_AUTHORITY_ID`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '用户和用户职权的映射表.'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of MAPPING_USER_TO_USER_AUTHORITY
-- ----------------------------
INSERT INTO `MAPPING_USER_TO_USER_AUTHORITY`
VALUES ('caplike', 0x6538616437376431356363303463333039373635386439343537313464363363);

SET FOREIGN_KEY_CHECKS = 1;
