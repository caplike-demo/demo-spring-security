/*
 Navicat Premium Data Transfer

 Source Server         : MySQL8#腾讯云-LiKe
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : 111.229.226.114:3306
 Source Schema         : demo_spring_security_oauth2

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 30/07/2020 11:01:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for MAPPING_USER_AUTHORITY_TO_RESOURCE
-- ----------------------------
DROP TABLE IF EXISTS `MAPPING_USER_AUTHORITY_TO_RESOURCE`;
CREATE TABLE `MAPPING_USER_AUTHORITY_TO_RESOURCE`
(
    `USER_AUTHORITY_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户职权 ID',
    `RESOURCE_ID`       varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '资源 ID',
    PRIMARY KEY (`USER_AUTHORITY_ID`, `RESOURCE_ID`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '用户职权和资源的映射表.'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of MAPPING_USER_AUTHORITY_TO_RESOURCE
-- ----------------------------
INSERT INTO `MAPPING_USER_AUTHORITY_TO_RESOURCE`
VALUES ('e8ad77d15cc04c3097658d945714d63c', 'c9f1dcc6effc432eb0c474b307b6cec3');

SET FOREIGN_KEY_CHECKS = 1;
