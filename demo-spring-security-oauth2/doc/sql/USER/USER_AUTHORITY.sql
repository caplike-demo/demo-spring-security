/*
 Navicat Premium Data Transfer

 Source Server         : MySQL8#腾讯云-LiKe
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : 111.229.226.114:3306
 Source Schema         : demo_spring_security_oauth2

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 30/07/2020 10:41:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for USER_AUTHORITY
-- ----------------------------
DROP TABLE IF EXISTS `USER_AUTHORITY`;
CREATE TABLE `USER_AUTHORITY`
(
    `ID`          varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '用户职权 ID',
    `NAME`        varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '职权名称',
    `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '职权描述',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '用户职权. 如 ADMIN, USER etc. 职权代表了一簇可访问的资源集 (RESOURCE).'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of USER_AUTHORITY
-- ----------------------------
INSERT INTO `USER_AUTHORITY`
VALUES ('e096cef2a3cf491f915dd25542b1218f', 'ADMIN', '管理员');
INSERT INTO `USER_AUTHORITY`
VALUES ('e8ad77d15cc04c3097658d945714d63c', 'USER', '用户');

SET FOREIGN_KEY_CHECKS = 1;
