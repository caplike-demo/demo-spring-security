/*
 Navicat Premium Data Transfer

 Source Server         : MySQL8#腾讯云-LiKe
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : 111.229.226.114:3306
 Source Schema         : demo_spring_security_oauth2

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 30/07/2020 10:05:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for CLIENT_AUTHORITY
-- ----------------------------
DROP TABLE IF EXISTS `CLIENT_AUTHORITY`;
CREATE TABLE `CLIENT_AUTHORITY`
(
    `ID`          varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '客户端职权 ID',
    `NAME`        varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '职权名称',
    `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '职权描述',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '客户端职权. 职权代表了一簇可访问的资源集 (RESOURCE).'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of CLIENT_AUTHORITY
-- ----------------------------
INSERT INTO `CLIENT_AUTHORITY`
VALUES ('11cffc50570a4866819cee58d695b703', 'THIRD_PARTY_CLIENT', '第三方客户端');
INSERT INTO `CLIENT_AUTHORITY`
VALUES ('4300f23105084396a86c3ff301e54e9e', 'FIRST_PARTY_FRONTEND_CLIENT', '第一方前端');
INSERT INTO `CLIENT_AUTHORITY`
VALUES ('bade7a63483640bab2d79cd2b78fdea8', 'FIRST_PARTY_BACKEND_CLIENT', '第一方后端');

SET FOREIGN_KEY_CHECKS = 1;
