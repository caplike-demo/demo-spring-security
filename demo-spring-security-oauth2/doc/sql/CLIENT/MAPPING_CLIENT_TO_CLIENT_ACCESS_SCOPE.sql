/*
 Navicat Premium Data Transfer

 Source Server         : MySQL8#腾讯云-LiKe
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : 111.229.226.114:3306
 Source Schema         : demo_spring_security_oauth2

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 30/07/2020 20:21:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for MAPPING_CLIENT_TO_CLIENT_ACCESS_SCOPE
-- ----------------------------
DROP TABLE IF EXISTS `MAPPING_CLIENT_TO_CLIENT_ACCESS_SCOPE`;
CREATE TABLE `MAPPING_CLIENT_TO_CLIENT_ACCESS_SCOPE`
(
    `CLIENT_ID`              varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '客户端 ID',
    `CLIENT_ACCESS_SCOPE_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '客户端访问范围 ID',
    PRIMARY KEY (`CLIENT_ID`, `CLIENT_ACCESS_SCOPE_ID`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of MAPPING_CLIENT_TO_CLIENT_ACCESS_SCOPE
-- ----------------------------
INSERT INTO `MAPPING_CLIENT_TO_CLIENT_ACCESS_SCOPE`
VALUES ('client-a', '9922e6a2b8314043a7fd174e8869922b');

SET FOREIGN_KEY_CHECKS = 1;
