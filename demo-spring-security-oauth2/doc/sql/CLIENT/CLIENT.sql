/*
 Navicat Premium Data Transfer

 Source Server         : MySQL8#腾讯云-LiKe
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : 111.229.226.114:3306
 Source Schema         : demo_spring_security_oauth2

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 31/07/2020 09:31:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for CLIENT
-- ----------------------------
DROP TABLE IF EXISTS `CLIENT`;
CREATE TABLE `CLIENT`
(
    `ID`                     varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '客户端 ID',
    `CLIENT_SECRET`          varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '客户端 Secret (加密后)',
    `AUTHORIZED_GRANT_TYPE`  varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NULL DEFAULT NULL COMMENT '授权方式, 只可能是: authorization_code,implicit,refresh_token,password,client_credentials.\r\n如果是多个, 以英文逗号分隔.',
    `REDIRECT_URI`           varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '重定向地址, 当授权方式是 authorization_code 时有效. 如果有多个, 按英文逗号分隔.',
    `ACCESS_TOKEN_VALIDITY`  int(0)                                                 NULL DEFAULT 120 COMMENT 'access-token 过期时间 (秒)',
    `REFRESH_TOKEN_VALIDITY` int(0)                                                 NULL DEFAULT 240 COMMENT 'refresh-token 过期时间 (秒)',
    `AUTO_APPROVE`           tinyint(1)                                             NULL DEFAULT 0 COMMENT '是否自动允许',
    `DESCRIPTION`            varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '客户端描述',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '客户端'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of CLIENT
-- ----------------------------
INSERT INTO `CLIENT`
VALUES ('client-a', '$2a$10$W0af8zbYneYlIBlWo.pkXue6K9cQTeAfTfRvt7J3.xbjPsuDAx146',
        'authorization_code,password,implicit,client_credentials,refresh_token', 'callback', 120, 240, 0,
        'client_secret: client-a-p');

SET FOREIGN_KEY_CHECKS = 1;
