/*
 Navicat Premium Data Transfer

 Source Server         : MySQL8#腾讯云-LiKe
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : 111.229.226.114:3306
 Source Schema         : demo_spring_security_oauth2

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 30/07/2020 10:24:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for MAPPING_CLIENT_TO_CLIENT_AUTHORITY
-- ----------------------------
DROP TABLE IF EXISTS `MAPPING_CLIENT_TO_CLIENT_AUTHORITY`;
CREATE TABLE `MAPPING_CLIENT_TO_CLIENT_AUTHORITY`
(
    `CLIENT_ID`           varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '客户端 ID',
    `CLIENT_AUTHORITY_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '客户端职权 ID',
    PRIMARY KEY (`CLIENT_ID`, `CLIENT_AUTHORITY_ID`) USING BTREE,
    INDEX `FK_MCTCA_CLIENT_AUTHORITY_ID_CLIENT_AUTHORITY` (`CLIENT_AUTHORITY_ID`) USING BTREE,
    CONSTRAINT `FK_MCTCA_CLIENT_AUTHORITY_ID_CLIENT_AUTHORITY` FOREIGN KEY (`CLIENT_AUTHORITY_ID`) REFERENCES `CLIENT_AUTHORITY` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_MCTCA_CLIENT_ID_CLIENT` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '客户端到客户端职权的映射表.'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of MAPPING_CLIENT_TO_CLIENT_AUTHORITY
-- ----------------------------
INSERT INTO `MAPPING_CLIENT_TO_CLIENT_AUTHORITY`
VALUES ('client-a', '11cffc50570a4866819cee58d695b703');

SET FOREIGN_KEY_CHECKS = 1;
