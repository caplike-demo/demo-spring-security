/*
 Navicat Premium Data Transfer

 Source Server         : MySQL8#腾讯云-LiKe
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : 111.229.226.114:3306
 Source Schema         : demo_spring_security_oauth2

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 30/07/2020 10:24:27
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for MAPPING_CLIENT_TO_RESOURCE_SERVER
-- ----------------------------
DROP TABLE IF EXISTS `MAPPING_CLIENT_TO_RESOURCE_SERVER`;
CREATE TABLE `MAPPING_CLIENT_TO_RESOURCE_SERVER`
(
    `CLIENT_ID`          varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '客户端 ID',
    `RESOURCE_SERVER_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '资源服务器 ID',
    PRIMARY KEY (`CLIENT_ID`, `RESOURCE_SERVER_ID`) USING BTREE,
    INDEX `FK_MCTRS_RESOURCE_SERVER_ID_RESOURCE_SERVER` (`RESOURCE_SERVER_ID`) USING BTREE,
    CONSTRAINT `FK_MCTRS_CLIENT_ID_CLIENT` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_MCTRS_RESOURCE_SERVER_ID_RESOURCE_SERVER` FOREIGN KEY (`RESOURCE_SERVER_ID`) REFERENCES `RESOURCE_SERVER` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '客户端到资源资源服务器的映射表. 标识了一个客户端可以访问的资源服务器.'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of MAPPING_CLIENT_TO_RESOURCE_SERVER
-- ----------------------------
INSERT INTO `MAPPING_CLIENT_TO_RESOURCE_SERVER`
VALUES ('client-a', 'resource-server');

SET FOREIGN_KEY_CHECKS = 1;
