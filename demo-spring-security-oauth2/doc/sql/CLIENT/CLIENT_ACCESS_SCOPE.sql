/*
 Navicat Premium Data Transfer

 Source Server         : MySQL8#腾讯云-LiKe
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : 111.229.226.114:3306
 Source Schema         : demo_spring_security_oauth2

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 31/07/2020 09:30:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for CLIENT_ACCESS_SCOPE
-- ----------------------------
DROP TABLE IF EXISTS `CLIENT_ACCESS_SCOPE`;
CREATE TABLE `CLIENT_ACCESS_SCOPE`
(
    `ID`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '客户端访问范围 ID',
    `SCOPE` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '客户端访问范围编码',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of CLIENT_ACCESS_SCOPE
-- ----------------------------
INSERT INTO `CLIENT_ACCESS_SCOPE`
VALUES ('9922e6a2b8314043a7fd174e8869922b', 'ACCESS_RESOURCE');

SET FOREIGN_KEY_CHECKS = 1;
