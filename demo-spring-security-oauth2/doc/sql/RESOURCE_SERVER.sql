/*
 Navicat Premium Data Transfer

 Source Server         : MySQL8#腾讯云-LiKe
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : 111.229.226.114:3306
 Source Schema         : demo_spring_security_oauth2

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 30/07/2020 09:51:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for RESOURCE_SERVER
-- ----------------------------
DROP TABLE IF EXISTS `RESOURCE_SERVER`;
CREATE TABLE `RESOURCE_SERVER`
(
    `ID`              varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '资源服务器 ID',
    `RESOURCE_SECRET` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '资源密钥 (加密后)',
    `DESCRIPTION`     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '资源服务器描述',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '资源服务器. 可提供客户端访问的资源服务器定义.'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of RESOURCE_SERVER
-- ----------------------------
INSERT INTO `RESOURCE_SERVER`
VALUES ('resource-server', 'resource-server-p', '资源服务器');

SET FOREIGN_KEY_CHECKS = 1;
