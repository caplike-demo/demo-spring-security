/*
 Navicat Premium Data Transfer

 Source Server         : MySQL8#腾讯云-LiKe
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : 111.229.226.114:3306
 Source Schema         : demo_spring_security_oauth2

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 31/07/2020 11:44:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for RESOURCE
-- ----------------------------
DROP TABLE IF EXISTS `RESOURCE`;
CREATE TABLE `RESOURCE`
(
    `ID`                 varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '标识资源的 ID',
    `ENDPOINT`           varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '资源端点, 支持通配符',
    `RESOURCE_SERVER_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '资源服务器 ID, 用以标识当前端点属于哪个资源服务.\r\n外键约束 RESOURCE_SERVER 的 ID.',
    PRIMARY KEY (`ID`) USING BTREE,
    INDEX `FK_R_RESOURCE_SERVER_ID_2_RS` (`RESOURCE_SERVER_ID`) USING BTREE,
    CONSTRAINT `FK_R_RESOURCE_SERVER_ID_2_RS` FOREIGN KEY (`RESOURCE_SERVER_ID`) REFERENCES `RESOURCE_SERVER` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '资源. 代表着形如 /user/1 的具体的资源本身.'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of RESOURCE
-- ----------------------------
INSERT INTO `RESOURCE`
VALUES ('c9f1dcc6effc432eb0c474b307b6cec3', '/resource/access', 'resource-server');

SET FOREIGN_KEY_CHECKS = 1;
