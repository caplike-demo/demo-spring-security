# About dynamic-authorization

[博文地址](https://blog.csdn.net/caplike/article/details/106185569)

在 Spring Security 中实现动态权限.  
更改用户权限需要重新登陆, 更改角色权限不需要.

基于 DEMO: UserDetailsService, jwt 和 csrf. 

~ 登陆请求会被 `JWTAuthenticationFilter` 过滤:

- **/auth/login** 登陆端点. 会经过 `CsrfFilter` 和 `JWTAuthenticationFilter`, 验证 inputStream 中有没有用户信息. 

  如果身份验证成功, 生成 access-token 和 csrf-token;

  如果失败, 不会生成 access-token 和 csrf-token, 虽然不会缓存 csrf-token,

---------

~ 其余所有端点都会被 `JWTAuthorizatonFilter` 过滤:

- **/auth/register** 注册端点. 被放行. 不会生成 csrf-token 和 access-token.

- **/admin/info** 有 ADMIN 权限的才能访问.

- **/user/1** 有 USER 权限的才能访问. 每次访问都需要 access-token 和 csrf-token, 且 access-token 的过期时间会被延后, csrf-token 每次请求都会变化.

## JWTAuthenticationFilter

`extends UsernamePasswordAuthenticationFilter`, 实现用户名和密码的验证.

 处理 URL: /auth/login, 意味着只有这个 URL 会通过这个过滤器.

**如果用户登陆成功, 就在响应头中置入 access-token**, key: \<name>.userinfo.access-token

## JWTAuthorizationFilter

`extends OncePerRequestFilter`, 对白名单 /auth/register 之外的每一个请求都会过滤: 验证请求头中是否有 access-token.

**每一次携带合法 access-token 的访问都会被放行, 并且会延长缓存中 access-token 的生命周期, 并更换 csrf-token**

先于 `AccessDecisionManager` 执行.

## CsrfFilter

@see ./demo-spring-security-csrf/README.md

---

为了降低代码复杂度, 仍然采用 access-token 和 csrf-token 分开缓存的方式. 且 csrf-token 不作失效, 只做更新: 每次成功的请求之后, 即请求到达 JWTAuthorizationFilter 之后, 更新 csrf-token.

key 的格式暂定为 \<name>.access-token = \<access-token> 和 \<name>.csrf-token = \<csrf-token>

当访问 /auth/login 端点的时候, 会首先调用 CsrfTokenRepository 的 loadToken 方法获取可能已经存在的 csrf-token; 如果这个方法返回 null, 紧接着会调用 generateToken 方法生成 csrf-token, 最后再执行 saveToken 方法将 csrf-token 保存在服务端

# Reference

[Spring Boot 项目在 Filter 中抛出异常处理方法](https://blog.csdn.net/youshouyiqi/article/details/86535001)

[Spring Boot 中获取所有 RequestMapping 的 URL 路径列表集](https://blog.csdn.net/tt____tt/article/details/82012999?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromBaidu-3&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromBaidu-3)

[44.4.6 How do I define the secured URLs within an application dynamically?](https://docs.spring.io/spring-security/site/docs/4.2.4.RELEASE/reference/htmlsingle/#appendix-faq-dynamic-url-metadata)

[Spring Security 动态配置 URL 权限认证](https://www.jianshu.com/p/0a06496e75ea)

[Spring Security 动态配置 URL 权限](https://segmentfault.com/a/1190000010672041)

[Spring Security 动态配置权限的方案 2](https://segmentfault.com/a/1190000012249623)

---

https://howtodoinjava.com/spring-restful/access-denied-json-response/

[Spring Boot 前后分离项目怎么处理 Spring Security 抛出的异常](https://my.oschina.net/liululee/blog/1808027)

# 更新日志

## 2020年5月6日10:00:03

The Very First Version.