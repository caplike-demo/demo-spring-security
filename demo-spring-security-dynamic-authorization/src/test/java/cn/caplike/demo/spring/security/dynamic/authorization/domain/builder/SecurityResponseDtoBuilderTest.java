package cn.caplike.demo.spring.security.dynamic.authorization.domain.builder;

import cn.caplike.demo.spring.security.dynamic.authorization.domain.dto.SecurityResponseDto;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.springframework.http.HttpStatus;

public class SecurityResponseDtoBuilderTest {

    @Test
    public void build() {
        final SecurityResponseDto dto = SecurityResponseDtoBuilder.of().with(builder -> {
            builder.httpStatus = HttpStatus.OK;
            builder.message = "OK";
            builder.data = Lists.list("1", "2");
        }).build();

        System.out.println(dto);
    }
}