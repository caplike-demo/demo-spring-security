package cn.caplike.demo.spring.security.dynamic.authorization.mapper;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RoleMapperTest {

    private RoleMapper roleMapper;

    @Test
    public void queryRolePopedomDto() {
        roleMapper.queryRolePopedomDto().stream().map(JSON::toJSONString).forEach(System.out::println);
    }

    // ~ Autowired
    // -----------------------------------------------------------------------------------------------------------------

    @Autowired
    public void setRoleMapper(RoleMapper roleMapper) {
        this.roleMapper = roleMapper;
    }
}