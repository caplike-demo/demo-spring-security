package cn.caplike.demo.spring.security.dynamic.authorization.mapper;

import cn.caplike.demo.spring.security.dynamic.authorization.domain.dto.UserRoleDto;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserMapperTest {

    private UserMapper userMapper;

    @Test
    public void getUserRoleDto() {
        final UserRoleDto userRoleDto = userMapper.getUserRoleDto("like");
        System.out.println(JSON.toJSONString(userRoleDto));
    }

    // ~ Autowired
    // -----------------------------------------------------------------------------------------------------------------

    @Autowired
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }
}