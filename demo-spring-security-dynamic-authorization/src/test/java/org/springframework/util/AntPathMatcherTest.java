package org.springframework.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * AntPathMatcherTest
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-07 13:00
 */
public class AntPathMatcherTest {

    // 可以用 @Bean @Autowired 注入
    private final AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Test
    public void test() {
        Assert.assertTrue(antPathMatcher.match("/user/*", "/user/1"));
        Assert.assertTrue(antPathMatcher.match("/user/*/a", "/user/1/a"));
        Assert.assertTrue(antPathMatcher.match("/user/**", "/user/1/a"));
    }

}
