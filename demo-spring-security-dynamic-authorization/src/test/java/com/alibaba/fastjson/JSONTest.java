package com.alibaba.fastjson;

import org.junit.Test;

/**
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-08 14:07
 */
public class JSONTest {

    @Test
    public void toNull() {
        System.out.println(JSON.toJSONString(null));
        System.out.println(JSON.toJSONString(new Object()));
    }
}
