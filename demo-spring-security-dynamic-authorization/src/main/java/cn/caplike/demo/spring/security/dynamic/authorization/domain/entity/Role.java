package cn.caplike.demo.spring.security.dynamic.authorization.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色实体
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-06 17:00
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Role {

    private int id;
    private String name;

}
