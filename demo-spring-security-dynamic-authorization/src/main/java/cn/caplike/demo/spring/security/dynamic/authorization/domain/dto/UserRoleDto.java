package cn.caplike.demo.spring.security.dynamic.authorization.domain.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * 用户角色 DTO
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-07 09:08
 */
@Data
@NoArgsConstructor
public class UserRoleDto {

    // ~ USER Fields
    // -----------------------------------------------------------------------------------------------------------------
    private String name;
    private String password;

    // ~ ROLE_USER Fields
    private Set<String> roles;

}
