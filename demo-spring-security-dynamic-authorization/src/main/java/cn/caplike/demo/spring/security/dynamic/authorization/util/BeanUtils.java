package cn.caplike.demo.spring.security.dynamic.authorization.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import java.util.Map;
import java.util.Objects;

/**
 * Bean 工具类<br>
 * 依赖 fastJson
 *
 * <dl>
 *     <dt><b>ChangeLog:</b></dt>
 *     <dd>1.0.0@2020-5-14 10:03 - The Very First Version.</dd>
 * </dl>
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-14 09:53
 */
@SuppressWarnings("unused")
public final class BeanUtils {

    private static final TypeReference<Map<String, Object>> mapTypeReference = new TypeReference<Map<String, Object>>() {
    };

    private BeanUtils() {
    }

    /**
     * Description: 对象转 {@link Map}
     *
     * @param target 对象
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @author LiKe
     * @date 2020-05-14 10:00:00
     */
    public static Map<String, Object> toMap(Object target) {
        if (Objects.requireNonNull(target, "Parameter target cannot be null.") instanceof String) {
            return JSON.parseObject((String) target, mapTypeReference);
        }
        return JSON.parseObject(JSON.toJSONString(target), mapTypeReference);
    }
}
