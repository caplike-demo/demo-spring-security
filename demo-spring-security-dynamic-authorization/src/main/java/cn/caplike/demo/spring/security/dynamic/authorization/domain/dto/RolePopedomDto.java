package cn.caplike.demo.spring.security.dynamic.authorization.domain.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色-权限 DTO
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-07 13:49
 */
@Data
@NoArgsConstructor
public class RolePopedomDto {

    /**
     * ROLE name
     */
    private String roleName;

    /**
     * POPEDOM request_url
     */
    private String requestUrl;

}
