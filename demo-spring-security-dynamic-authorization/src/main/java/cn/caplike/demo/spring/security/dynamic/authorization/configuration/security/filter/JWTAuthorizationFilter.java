package cn.caplike.demo.spring.security.dynamic.authorization.configuration.security.filter;

import cn.caplike.data.redis.service.spring.boot.starter.RedisKey;
import cn.caplike.data.redis.service.spring.boot.starter.RedisService;
import cn.caplike.demo.spring.security.dynamic.authorization.configuration.security.SecurityConfiguration;
import cn.caplike.demo.spring.security.dynamic.authorization.domain.dto.CustomUserDetailsDto;
import cn.caplike.demo.spring.security.dynamic.authorization.util.AccessTokenUtils;
import cn.caplike.demo.spring.security.dynamic.authorization.util.RequestUtils;
import cn.caplike.demo.spring.security.dynamic.authorization.util.ResponseUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 鉴权过滤器: 用于校验请求头中有没有合法的 access-token<br>
 * 除了 /auth/login 之外的所有请求都会经过这个过滤器<br>
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-08 09:21
 */
@Slf4j
public class JWTAuthorizationFilter extends OncePerRequestFilter {

    private static final String UNAUTHORIZED_MESSAGE = "Unauthorized access.";

    /**
     * access-token 过期 或者 不是有效的 access-token
     */
    private static final String ACCESS_TOKEN_EXPIRED_MESSAGE = "Invalid access-token.";

    /**
     * 白名单, 放行 /auth/register 注册端点
     */
    private static final Set<String> WHITE_LIST = Stream.of(SecurityConfiguration.REGISTER_URI).collect(Collectors.toSet());

    /**
     * {@link RedisService }
     */
    private final RedisService redisService;

    public JWTAuthorizationFilter(RedisService redisService) {
        this.redisService = redisService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        final String authorization = request.getHeader(AccessTokenUtils.AUTHORIZATION_HEADER);

        // ~ without access-token
        // -------------------------------------------------------------------------------------------------------------
        if (WHITE_LIST.contains(RequestUtils.getQualifiedURI(request))) {
            filterChain.doFilter(request, response);
            return;
        }

        if (StringUtils.isBlank(authorization)) {
            ResponseUtils.unauthorizedResponse(response, UNAUTHORIZED_MESSAGE);
            return;
        }

        // ~ with access-token
        // -------------------------------------------------------------------------------------------------------------

        final String jwt = authorization.replaceFirst(AccessTokenUtils.BEARER_TOKEN_TYPE, StringUtils.EMPTY);
        final CustomUserDetailsDto customUserDetailsDto = AccessTokenUtils.getCustomUserDetails(jwt);

        // 如果 access-token 已经失效
        final RedisKey accessTokenKey = RedisKey.builder().prefix(customUserDetailsDto.getName()).suffix(SecurityConfiguration.ACCESS_TOKEN).build();
        if (Objects.isNull(redisService.getValue(accessTokenKey, String.class))) {
            ResponseUtils.unauthorizedResponse(response, ACCESS_TOKEN_EXPIRED_MESSAGE);
            return;
        }

        // 每次授权的访问, 都延长 access-token 的过期时间, 返回新的 token
        redisService.expire(accessTokenKey, AccessTokenUtils.LIFE_TIME);

        // 认证
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
                customUserDetailsDto.getName(),
                customUserDetailsDto.getPassword(),
                customUserDetailsDto.getAuthorities()
        ));

        filterChain.doFilter(request, response);
    }
}
