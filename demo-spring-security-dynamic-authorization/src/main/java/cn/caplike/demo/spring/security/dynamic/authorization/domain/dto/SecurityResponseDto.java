package cn.caplike.demo.spring.security.dynamic.authorization.domain.dto;

import cn.caplike.demo.spring.security.dynamic.authorization.util.TimeUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;

/**
 * 安全框架返回对象封装
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-08 10:12
 */
@Getter
@Setter
@AllArgsConstructor
public class SecurityResponseDto {

    /**
     * {@link HttpStatus}
     */
    private HttpStatus httpStatus;

    /**
     * 消息
     */
    private String message;

    /**
     * 时间戳
     */
    private LocalDateTime timestamp;

    /**
     * 数据
     */
    private Object data;

    /**
     * @return { timestamp: '', status: '', message: '', data: {} }
     */
    @Override
    public String toString() {
        // 使用 LinkedHashMap 保证序列化顺序
        return new JSONObject(new LinkedHashMap<>(4))
                .fluentPut("timestamp", TimeUtils.format("yyyy-MM-dd HH:mm:ss", this.timestamp))
                .fluentPut("status", this.httpStatus.value())
                .fluentPut("message", this.message)
                .fluentPut("data", JSON.toJSONString(data))
                .toString();
    }
}
