package cn.caplike.demo.spring.security.dynamic.authorization.configuration.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 系统配置属性
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-09 08:59
 */
@Component
public final class ApplicationConfiguration {

    /**
     * 系统上下文, 形如 /dynamic-authorization
     */
    public static String CONTEXT_PATH;

    private ApplicationConfiguration() {
    }

    @Value("${server.servlet.context-path}")
    public void setContextPath(String contextPath) {
        CONTEXT_PATH = contextPath;
    }
}
