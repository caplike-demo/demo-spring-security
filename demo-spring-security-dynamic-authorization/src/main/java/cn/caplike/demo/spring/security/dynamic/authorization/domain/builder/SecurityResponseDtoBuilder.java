package cn.caplike.demo.spring.security.dynamic.authorization.domain.builder;

import cn.caplike.demo.spring.security.dynamic.authorization.domain.dto.SecurityResponseDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * {@link SecurityResponseDto} Builder
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-08 10:32
 */
@SuppressWarnings("unused")
public final class SecurityResponseDtoBuilder {

    private static final Object EMPTY_DATA = new Object();

    /**
     * SecurityResponseDto#httpStatus
     */
    public HttpStatus httpStatus;

    /**
     * SecurityResponseDto#message
     */
    public String message;

    /**
     * SecurityResponse#timestamp
     */
    public LocalDateTime timestamp;

    /**
     * SecurityResponse#data
     */
    public Object data;

    private SecurityResponseDtoBuilder() {
    }

    /**
     * Description: 获取构造器的实例对象
     *
     * @return cn.caplike.demo.spring.security.dynamic.authorization.domain.builder.SecurityResponseDtoBuilder
     * @author LiKe
     * @date 2020-05-08 13:56:55
     */
    public static SecurityResponseDtoBuilder of() {
        return new SecurityResponseDtoBuilder();
    }

    public SecurityResponseDtoBuilder with(Consumer<SecurityResponseDtoBuilder> builderConsumer) {
        builderConsumer.accept(this);
        return this;
    }

    /**
     * Description: 构建目标对象的实例
     *
     * @return cn.caplike.demo.spring.security.dynamic.authorization.domain.dto.SecurityResponseDto
     * @author LiKe
     * @date 2020-05-08 13:59:19
     */
    public SecurityResponseDto build() {
        return new SecurityResponseDto(
                Optional.ofNullable(httpStatus).orElse(HttpStatus.OK),
                StringUtils.isBlank(message) ? StringUtils.EMPTY : message,
                Optional.ofNullable(timestamp).orElse(LocalDateTime.now()),
                Optional.ofNullable(data).orElse(EMPTY_DATA)
        );
    }
}
