package cn.caplike.demo.spring.security.dynamic.authorization.util;

import cn.caplike.demo.spring.security.dynamic.authorization.configuration.security.ApplicationConfiguration;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * RequestUtils
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-13 13:29
 */
public final class RequestUtils {

    private RequestUtils() {
    }

    /**
     * Description: 获得有效的 requestURI
     *
     * @param request {@link HttpServletRequest}
     * @return java.lang.String
     * @author LiKe
     * @date 2020-05-13 13:30:58
     */
    public static String getQualifiedURI(HttpServletRequest request) {
        return StringUtils.replace(request.getRequestURI(), ApplicationConfiguration.CONTEXT_PATH, StringUtils.EMPTY);
    }
}
