package cn.caplike.demo.spring.security.dynamic.authorization.mapper;

import cn.caplike.demo.spring.security.dynamic.authorization.domain.dto.RolePopedomDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * ROLE Mapper
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-07 09:35
 */
@Mapper
@Repository
public interface RoleMapper {

    @Select("SELECT " +
            "   r.name " +
            "FROM " +
            "   ROLE r " +
            "WHERE " +
            "   r.id IN ( SELECT ru.role_id FROM ROLE_USER ru WHERE ru.user_id = #{userId} )")
    Set<String> queryRoleName(int userId);

    /**
     * Description: 获取角色名称和权限 URL 的对应关系
     *
     * @return java.util.List<cn.caplike.demo.spring.security.dynamic.authorization.domain.dto.RolePopedomDto>
     * @author LiKe
     * @date 2020-05-07 14:13:36
     */
    @Select("SELECT " +
            "   r.name as role_name, " +
            "   rpp.request_url " +
            "FROM " +
            "   ROLE r" +
            "   LEFT JOIN ( SELECT rp.role_id, p.request_url FROM ROLE_POPEDOM rp LEFT JOIN POPEDOM p ON rp.popedom_id = p.id ) rpp ON r.id = rpp.role_id")
    List<RolePopedomDto> queryRolePopedomDto();

}
