package cn.caplike.demo.spring.security.dynamic.authorization.util;

import org.apache.commons.lang3.StringUtils;

import java.util.UUID;

/**
 * csrf-token 工具类
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-13 13:14
 */
public final class CsrfTokenUtils {

    private CsrfTokenUtils() {
    }

    /**
     * Description: 生成 csrf-token
     *
     * @return java.lang.String csrf-token
     * @author LiKe
     * @date 2020-05-13 13:15:16
     */
    public static String create() {
        return StringUtils.replace(UUID.randomUUID().toString(), "-", StringUtils.EMPTY);
    }
}
