package cn.caplike.demo.spring.security.dynamic.authorization.configuration.security.filter;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 将 HttpServletRequest 用 RequestBodyServletRequestWrapper 包装的过滤器
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-02 10:36
 */
@Slf4j
public class HttpServletRequestWrapFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        log.info("request body servlet request wrap filter ...");
        final RequestBodyServletRequestWrapper requestWrapper = new RequestBodyServletRequestWrapper(request);

        filterChain.doFilter(requestWrapper, response);
    }

    /**
     * ServletRequest 的包装器 (让后续方法可以重复调用 request.getInputStream())<br>
     * 处理流只能读取一次的问题, 用包装器继续将流写出. @RequestBody 会调用 getInputStream 方法, 所以本质上是解决 getInputStream 多次调用的问题: <br>
     * ServletRequest 的 getReader() 和 getInputStream() 两个方法只能被调用一次，而且不能两个都调用。那么如果 Filter 中调用了一次，在 Controller 里面就不能再调用了,
     * 会抛出异常:getReader() has already been called for this request 异常
     *
     * @author LiKe
     * @date 2019-11-21 09:24
     */
    private static class RequestBodyServletRequestWrapper extends HttpServletRequestWrapper {

        /**
         * 请求体数据
         */
        private final byte[] requestBody;
        /**
         * 重写的参数 Map
         */
        private final Map<String, String[]> paramMap;

        public RequestBodyServletRequestWrapper(HttpServletRequest request) throws IOException {
            super(request);

            // 重写 requestBody
            requestBody = IOUtils.toByteArray(request.getReader(), StandardCharsets.UTF_8);

            // 重写参数 Map
            paramMap = new HashMap<>();

            if (requestBody.length == 0) {
                return;
            }

            JSON.parseObject(getRequestBody()).forEach((key, value) -> paramMap.put(key, new String[]{String.valueOf(value)}));
        }

        public String getRequestBody() {
            return StringUtils.toEncodedString(requestBody, StandardCharsets.UTF_8);
        }

        // ~ get
        // -----------------------------------------------------------------------------------------------------------------

        @Override
        public Map<String, String[]> getParameterMap() {
            return paramMap;
        }

        @Override
        public String getParameter(String key) {
            String[] valueArr = paramMap.get(key);
            if (valueArr == null || valueArr.length == 0) {
                return null;
            }
            return valueArr[0];
        }

        @Override
        public String[] getParameterValues(String key) {
            return paramMap.get(key);
        }

        @Override
        public Enumeration<String> getParameterNames() {
            return Collections.enumeration(paramMap.keySet());
        }

        // ~ read
        // -----------------------------------------------------------------------------------------------------------------

        @Override
        public BufferedReader getReader() {
            return new BufferedReader(new InputStreamReader(getInputStream()));
        }

        @Override
        public ServletInputStream getInputStream() {
            final ByteArrayInputStream inputStream = new ByteArrayInputStream(requestBody);
            return new ServletInputStream() {
                @Override
                public boolean isFinished() {
                    return false;
                }

                @Override
                public boolean isReady() {
                    return false;
                }

                @Override
                public void setReadListener(ReadListener listener) {

                }

                @Override
                public int read() {
                    return inputStream.read();
                }
            };
        }
    }
}
