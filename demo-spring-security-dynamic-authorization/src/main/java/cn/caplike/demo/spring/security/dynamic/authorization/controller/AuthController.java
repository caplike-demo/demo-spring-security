package cn.caplike.demo.spring.security.dynamic.authorization.controller;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * AuthController
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-07 14:47
 */
@Slf4j
@RequestMapping("/auth")
@RestController
public class AuthController {
    private static final String CLASS_NAME = "AuthController";

    @PostMapping("/register")
    public void register(@RequestBody Map<String, String> userInfo) {
        log.debug("{}#register :: 用户注册接口, 用户信息: {}", CLASS_NAME, JSON.toJSONString(userInfo));
    }

}
