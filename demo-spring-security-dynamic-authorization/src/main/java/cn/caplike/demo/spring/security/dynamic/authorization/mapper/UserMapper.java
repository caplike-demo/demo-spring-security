package cn.caplike.demo.spring.security.dynamic.authorization.mapper;

import cn.caplike.demo.spring.security.dynamic.authorization.domain.dto.UserRoleDto;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * USER Mapper
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-07 09:18
 */
@Mapper
@Repository
public interface UserMapper {

    /**
     * Description: 获取 用户-角色s 对象
     *
     * @param username 用户名
     * @return cn.caplike.demo.spring.security.dynamic.authorization.domain.dto.UserRoleDto
     * @author LiKe
     * @date 2020-05-12 13:51:58
     */
    @Select("SELECT * FROM USER where name = #{username}")
    @Results({
            @Result(property = "name", column = "name"),
            @Result(property = "password", column = "password"),
            @Result(property = "roles", column = "id", javaType = Set.class,
                    many = @Many(
                            select = "cn.caplike.demo.spring.security.dynamic.authorization.mapper.RoleMapper.queryRoleName",
                            fetchType = FetchType.EAGER
                    )
            )
    })
    UserRoleDto getUserRoleDto(String username);

}
