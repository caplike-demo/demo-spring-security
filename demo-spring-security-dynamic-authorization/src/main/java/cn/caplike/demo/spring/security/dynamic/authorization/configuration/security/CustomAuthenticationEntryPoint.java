package cn.caplike.demo.spring.security.dynamic.authorization.configuration.security;

import cn.caplike.demo.spring.security.dynamic.authorization.util.ResponseUtils;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义的 AuthenticationEntryPoint
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-08 17:50
 */
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        ResponseUtils.unauthorizedResponse(response, authException.getMessage());
    }

}
