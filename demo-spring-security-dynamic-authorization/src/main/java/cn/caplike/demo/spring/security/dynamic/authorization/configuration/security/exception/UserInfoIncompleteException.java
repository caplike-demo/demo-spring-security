package cn.caplike.demo.spring.security.dynamic.authorization.configuration.security.exception;

/**
 * 用户信息不完整的异常, 在 Filter 中抛出.
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-14 09:44
 */
public class UserInfoIncompleteException extends RuntimeException {

    public UserInfoIncompleteException(String message) {
        super(message);
    }
}
