package cn.caplike.demo.spring.security.dynamic.authorization.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * UserController
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-06 14:14
 */
@RequestMapping("/user")
@RestController
public class UserController {

    @PostMapping("/{id}")
    public String get(@PathVariable String id) {
        return "UserController: " + id;
    }

}
