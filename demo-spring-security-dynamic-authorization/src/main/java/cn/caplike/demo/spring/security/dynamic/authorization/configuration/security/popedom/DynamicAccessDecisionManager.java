package cn.caplike.demo.spring.security.dynamic.authorization.configuration.security.popedom;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * {@link AccessDecisionManager} 的实现
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-06 10:27
 */
@Slf4j
@Component
public class DynamicAccessDecisionManager implements AccessDecisionManager {
    private static final String CLASS_NAME = "Dynamic access decision manager";

    @Override
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException, InsufficientAuthenticationException {
        // log.debug("{}#authentication: {}", CLASS_NAME, JSON.toJSONString(authentication));

        // In this case, object always be FilterInvocation
        // log.debug("{}#object: {}", CLASS_NAME, object.getClass().getCanonicalName());

        // 根据 requestUrl 过滤后的角色列表
        // log.debug("{}#configAttributes: {}", CLASS_NAME, JSON.toJSONString(configAttributes));

        final Set<String> requiredRolesByTargetResources = configAttributes.stream().map(ConfigAttribute::getAttribute).collect(Collectors.toSet());
        final Collection<? extends GrantedAuthority> incomingRequestAuthorities = authentication.getAuthorities();
        for (GrantedAuthority grantedAuthority : incomingRequestAuthorities) {
            // 如果当前 Authentication 有 requestUrl 的权限, 则认证通过
            // TODO 用 AntPathMatcher 匹配C
            if (requiredRolesByTargetResources.contains(grantedAuthority.getAuthority())) {
                log.debug("{}#decide :: authenticate success.", CLASS_NAME);
                return;
            }
        }

        // TODO 需要查阅源码
        // 如果不执行 SecurityContextHolder.clearContext() 即使抛出 AccessDeniedException,
        // 最终 httpSecurity.exceptionHandling 也会捕获到 InsufficientAuthenticationException
        SecurityContextHolder.clearContext();
        throw new InsufficientAuthenticationException("权限不足");
    }

    @Override
    public boolean supports(ConfigAttribute attribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        log.debug("{}#supports {}", CLASS_NAME, JSON.toJSONString(clazz));
        return true;
    }
}
