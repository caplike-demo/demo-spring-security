package cn.caplike.demo.spring.security.dynamic.authorization.configuration.security;

import cn.caplike.demo.spring.security.dynamic.authorization.util.ResponseUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.csrf.InvalidCsrfTokenException;
import org.springframework.security.web.csrf.MissingCsrfTokenException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义的 AccessDeniedHandler
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-08 17:48
 */
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException {

        if (accessDeniedException instanceof MissingCsrfTokenException) {
            ResponseUtils.forbiddenResponse(response, "Require csrf-token.");
        }

        if (accessDeniedException instanceof InvalidCsrfTokenException) {
            ResponseUtils.forbiddenResponse(response, "Invalid csrf-token.");
            return;
        }

        ResponseUtils.forbiddenResponse(response, accessDeniedException.getMessage());
    }

}
