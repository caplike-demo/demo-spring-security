package cn.caplike.demo.spring.security.dynamic.authorization.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 权限定义实体
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-06 16:55
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Popedom {

    private int id;
    private String requestUrl;

}
