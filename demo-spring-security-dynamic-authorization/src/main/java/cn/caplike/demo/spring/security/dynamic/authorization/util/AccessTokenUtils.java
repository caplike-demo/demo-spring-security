package cn.caplike.demo.spring.security.dynamic.authorization.util;

import cn.caplike.demo.spring.security.dynamic.authorization.domain.dto.CustomUserDetailsDto;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

/**
 * Json Web Token 工具类<br>
 * 不携带过期信息, 后者交由 Redis 管理
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-07 15:57
 */
public final class AccessTokenUtils {

    public static final String AUTHORIZATION_HEADER = "Authorization";

    public static final String BEARER_TOKEN_TYPE = "Bearer ";

    /**
     * access-token 的有效时长 (秒)
     */
    public static final long LIFE_TIME = 60 * 60;

    private static final String SECRET = "5ae95dd3c5f811b9b819434910c52820ae7cfb3d9f7961e7117b24a7012873767d79f61f81fc2e06ebb6fd4f09ab47764d6e20607f843c22a0a2a6a6ed829680";

    /**
     * 签发人
     */
    private static final String ISSUER = "caplike";

    private AccessTokenUtils() {
    }

    /**
     * Description: 获取 Subject
     *
     * @param jwt Json Web Token
     * @return java.lang.String
     * @author LiKe
     * @date 2020-05-07 16:13:35
     */
    public static String getSubject(String jwt) {
        return claims(jwt).getSubject();
    }

    /**
     * Description: 创建 Json Web Token<br>
     * Details: 用户密码会被 [PROTECTED] 遮盖
     *
     * @param customUserDetailsDto {@link CustomUserDetailsDto}
     * @return java.lang.String Json Web Token
     * @author LiKe
     * @date 2020-05-07 16:03:33
     */
    public static String create(CustomUserDetailsDto customUserDetailsDto) {
        customUserDetailsDto.setPassword("[PROTECTED]");
        return Jwts.builder()
                .setClaims(JSON.parseObject(JSON.toJSONString(customUserDetailsDto)))
                .setSubject(customUserDetailsDto.getName())
                .setIssuedAt(Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()))
                .setIssuer(ISSUER)
                .signWith(Keys.hmacShaKeyFor(SECRET.getBytes()), SignatureAlgorithm.HS512)
                .serializeToJsonWith(map -> JSON.toJSONBytes(map))
                .compact();
    }

    /**
     * Description: 从 JWT 中获取 {@link CustomUserDetailsDto}<br>
     * Details: 用户密码会被 [PROTECTED] 遮盖
     *
     * @param jwt Json Web Token
     * @return cn.caplike.demo.spring.security.dynamic.authorization.domain.dto.CustomUserDetails
     * @author LiKe
     * @date 2020-05-07 16:19:41
     */
    public static CustomUserDetailsDto getCustomUserDetails(String jwt) {
        return JSON.parseObject(JSON.toJSONString(claims(jwt)), CustomUserDetailsDto.class);
    }

    private static Claims claims(String jwt) {
        return Jwts.parserBuilder()
                .setSigningKey(Keys.hmacShaKeyFor(SECRET.getBytes()))
                .deserializeJsonWith(bytes -> JSONObject.parseObject(new String(bytes), new TypeReference<Map<String, Object>>() {
                }))
                .build()
                .parseClaimsJws(jwt)
                .getBody();
    }
}
