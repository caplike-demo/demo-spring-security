package cn.caplike.demo.spring.security.dynamic.authorization.controller;

import cn.caplike.demo.spring.security.dynamic.authorization.configuration.security.exception.UserInfoIncompleteException;
import cn.caplike.demo.spring.security.dynamic.authorization.domain.builder.SecurityResponseDtoBuilder;
import cn.caplike.demo.spring.security.dynamic.authorization.util.BeanUtils;
import cn.caplike.demo.spring.security.dynamic.authorization.util.TimeUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * {@link BasicErrorController }<br>
 * Ref: https://stackoverflow.com/questions/34366964/spring-boot-controlleradvice-exception-handler-not-firing
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-13 17:09
 */
@RestController
public class CustomErrorController extends BasicErrorController {

    private static final String EXCEPTION = "javax.servlet.error.exception";

    // private static final String STATUS_CODE_KEY = "javax.servlet.error.status_code";

    private static final String EXCEPTION_MESSAGE = "message";

    private static final String TIMESTAMP = "timestamp";

    public CustomErrorController(ErrorAttributes errorAttributes) {
        super(errorAttributes, new CustomErrorProperties());
    }

    @RequestMapping
    public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
        return responseEntity(request);
    }

    private ResponseEntity<Map<String, Object>> responseEntity(HttpServletRequest request) {
        final Map<String, Object> body = getErrorAttributes(request, false);
        final Object exception = request.getAttribute(EXCEPTION);

        // ~ UserInfoIncompleteException responseEntity
        if (exception instanceof UserInfoIncompleteException) {
            return new ResponseEntity<>(
                    BeanUtils.toMap(
                            SecurityResponseDtoBuilder.of().with(builder -> {
                                builder.timestamp = TimeUtils.fromDate((Date) MapUtils.getObject(body, TIMESTAMP));
                                builder.httpStatus = HttpStatus.FORBIDDEN;
                                builder.message = MapUtils.getString(body, EXCEPTION_MESSAGE);
                            }).build().toString()
                    ),
                    HttpStatus.FORBIDDEN
            );
        }

        return super.error(request);
    }

    /**
     * {@link ErrorProperties}
     */
    private static class CustomErrorProperties extends ErrorProperties {
        public CustomErrorProperties() {
            // 在 HttpServletRequest 中加入属性 javax.servlet.error.exception, 指明发生的具体异常
            super.setIncludeException(true);
        }
    }
}
