package cn.caplike.demo.spring.security.dynamic.authorization;

import com.alibaba.fastjson.parser.ParserConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * demo-spring-security-dynamic-authorization 启动类
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-06 09:46
 */
@MapperScan(basePackages = "cn.caplike.demo.spring.security.dynamic.authorization.mapper")
@SpringBootApplication
public class DynamicAuthorizationApplication {

    public static void main(String[] args) {
        SpringApplication.run(DynamicAuthorizationApplication.class);

        // https://github.com/alibaba/fastjson/wiki/enable_autotype
        ParserConfig.getGlobalInstance().addAccept("cn.caplike.demo.spring.security.dynamic.authorization.domain.");
        ParserConfig.getGlobalInstance().addAccept("cn.caplike.demo.spring.security.dynamic.authorization.configuration.security.exception.");
    }
}
