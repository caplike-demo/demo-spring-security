package cn.caplike.demo.spring.security.dynamic.authorization.controller;

import com.alibaba.fastjson.JSON;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * AdminController
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-05-06 14:44
 */
@RequestMapping("/admin")
@RestController
public class AdminController {

    /**
     * Description: 刷新权限缓存
     *
     * @return java.lang.String
     * @author LiKe
     * @date 2020-05-12 13:57:19
     */
    @GetMapping("/info")
    public String info() {
        // 匿名用户:
        // {
        //      "authenticated":true,
        //      "authorities":[
        //          {
        //              "authority":"ROLE_ANONYMOUS"
        //          }
        //      ],
        //      "credentials":"",
        //      "details": {
        //              "remoteAddress":"0:0:0:0:0:0:0:1"
        //          },
        //      "keyHash":332988589,
        //      "name":"anonymousUser",
        //      "principal":"anonymousUser"
        // }
        return JSON.toJSONString(SecurityContextHolder.getContext().getAuthentication());
    }

}
