package cn.caplike.demo.spring.security.quickstart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Quickstart 示例
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-04-15 12:57
 */
@SpringBootApplication
public class QuickstartApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuickstartApplication.class);
    }
}
