# About demo-spring-security-quickstart

本例作为 Spring Security 5 的快速入门案例。主要涉及 HttpSecurity 的 csrf，httpBasic，failureHandler 和 successHandler。

[博客地址](https://blog.csdn.net/caplike/article/details/105888635)

# Reference

[Ajax登陆，使用Spring Security缓存跳转到登陆前的链接](https://www.cnblogs.com/gobyte/p/10754273.html)

# 更新日志

## 2020-4-16 17:26:46

​		The Very First Version.

