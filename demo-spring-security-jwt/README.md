# About demo-spring-security-jwt

用 Json Web Token 实现用户身份和权限认证.

[博文地址](https://blog.csdn.net/caplike/article/details/105925711)

## AbstractAuthenticationProcessingFilter

实现类: `UsernamePasswordAuthenticationFilter`

```java
public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) req;
    HttpServletResponse response = (HttpServletResponse) res;

    if (!requiresAuthentication(request, response)) {
        chain.doFilter(request, response);

        return;
    }

    Authentication authResult;

    try {
        authResult = attemptAuthentication(request, response);
        if (authResult == null) {
            return;
        }
        sessionStrategy.onAuthentication(authResult, request, response);
    }
    catch (InternalAuthenticationServiceException failed) {
        unsuccessfulAuthentication(request, response, failed);
        return;
    }
    catch (AuthenticationException failed) {
        // Authentication failed
        unsuccessfulAuthentication(request, response, failed);
        return;
    }

    // Authentication success
    if (continueChainBeforeSuccessfulAuthentication) {
        chain.doFilter(request, response);
    }

    successfulAuthentication(request, response, chain, authResult);
}
```



# Reference

[从源码看Spring Security的角色和权限之区别](https://blog.csdn.net/cauchy6317/article/details/85162225)

[Spring Boot Security + JWT ''Hello World'' Example](https://dzone.com/articles/spring-boot-security-json-web-tokenjwt-hello-world)

[Springboot+Spring-Security+JWT+Redis实现restful Api的权限管理以及token管理（超详细用爱发电版）](https://blog.csdn.net/ech13an/article/details/80779973?depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1&utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1)

# 更新记录

## 2020年4月26日11:03:16

The Very First Version.