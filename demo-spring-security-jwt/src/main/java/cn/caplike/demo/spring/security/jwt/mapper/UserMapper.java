package cn.caplike.demo.spring.security.jwt.mapper;

import cn.caplike.demo.spring.security.jwt.domain.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * UserMapper
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-04-22 08:57
 */
@Repository
@Mapper
public interface UserMapper {

    /**
     * Description: 查找 User
     *
     * @param username 用户名
     * @return cn.caplike.demo.spring.security.jwt.domain.entity.User
     * @author LiKe
     * @date 2020-04-22 09:04:06
     */
    @Select("SELECT * FROM USER WHERE name = #{username}")
    User findByUsername(String username);

    /**
     * Description: 新建 User
     *
     * @param user {@link User}
     * @return cn.caplike.demo.spring.security.jwt.domain.entity.User
     * @author LiKe
     * @date 2020-04-22 11:18:21
     */
    @Insert("INSERT INTO USER(name, password, role) VALUES (#{name}, #{password}, #{role})")
    int save(User user);
}
