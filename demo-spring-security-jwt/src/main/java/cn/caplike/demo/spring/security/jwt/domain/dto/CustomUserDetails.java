package cn.caplike.demo.spring.security.jwt.domain.dto;

import cn.caplike.demo.spring.security.jwt.domain.entity.User;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collections;
import java.util.Set;

/**
 * JwtUser
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-04-22 09:22
 */
@Data
@NoArgsConstructor
public class CustomUserDetails implements UserDetails {

    /**
     * username
     */
    private String name;

    /**
     * password
     */
    private String password;

    /**
     * {@link GrantedAuthority}s
     */
    private Set<SimpleGrantedAuthority> authorities;

    public CustomUserDetails(@NonNull User user) {
        this.name = user.getName();
        this.password = user.getPassword();
        this.authorities = Collections.singleton(new SimpleGrantedAuthority(user.getRole()));
    }

    @Override
    public Set<SimpleGrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
