package cn.caplike.demo.spring.security.jwt;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * demo-spring-security-jwt 启动类
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-04-21 14:25
 */
@SpringBootApplication
@MapperScan(basePackages = "cn.caplike.demo.spring.security.jwt.mapper")
public class JWTApplication {

    public static void main(String[] args) {
        SpringApplication.run(JWTApplication.class);
    }
}
