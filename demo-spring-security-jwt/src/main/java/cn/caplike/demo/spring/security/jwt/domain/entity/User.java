package cn.caplike.demo.spring.security.jwt.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-04-17 10:41
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private String name;
    private String password;

    /**
     * 用户角色
     */
    private String role;

}
