package cn.caplike.demo.spring.security.jwt.util;

import cn.caplike.demo.spring.security.jwt.domain.dto.CustomUserDetails;
import cn.caplike.demo.spring.security.jwt.domain.entity.User;
import org.junit.Test;

public class JWTUtilsTest {

    @Test
    public void create() {
        System.out.println(JWTUtils.create("like", false, new CustomUserDetails(new User("LiKe", "12345", "USER"))));
    }

    @Test
    public void subject() {
        System.out.println(
                JWTUtils.subject(JWTUtils.create("like", false, new CustomUserDetails(new User("LiKe", "12345", "USER"))))
        );
    }

    @Test
    public void userDetails() {
        System.out.println(
                JWTUtils.userDetails(JWTUtils.create("like", false, new CustomUserDetails(new User("LiKe", "12345", "USER"))))
        );
    }
}