# About demo-spring-security-csrf

测试 CSRF TOKEN REPOSITORY

## RedisCsrfTokenRepository

由于 (启用了 jwt 之后) 设置了禁用 session, 所以 csrf-token 没法和 session 绑定了, 考虑将 csrf-token 放到 redis 中. ~~如下所示, 绑定到一个带过期时间的 key 上, 与 jwt 同生命周期.~~

```json
"<user-id>": {
	"access-token": "",
	"csrf-token": ""
}
```

```properties
# access-token 和 csrf-token 分开缓存的方式
<name>.user-info.access-token=<access-token>
<name>.user-info.csrf-token=<csrf-token>
```

~~每次请求更新 csrf-token 或者 csrf-token 随着 jwt 共享相同的生命周期.~~ csrf-token 和 access-token 单独保存, 这样降低代码复杂度.

登陆成功后会生成 access-token 和 csrf-token 一起返回给前端, 前端带着这两个 token 访问后端, 在没有过期的前提下, 放行并延长生命周期; 如果过期, 提示重新登陆.

## CsrfFilter

放行 /auth/login 端点, 是 Spring Security 实现的用于校验 csrf-token 的过滤器, 该过滤器先于 UsernamePasswordAuthenticationFilter 执行.

当访问 /auth/login 端点的时候, 会首先调用 CsrfTokenRepository 的 loadToken 方法获取可能已经存在的 csrf-token; 如果这个方法返回 null, 紧接着会调用 generateToken 方法生成 csrf-token, 最后再执行 saveToken 方法将 csrf-token 保存在服务端. 如果 loadToken 返回服务端保存的 csrf-token, 则会直接和请求头中的 csrf-token 比较, 相等就放行. 否则调用 accessDeniedHandler. 摘录 (CsrfFilter) 相关源码如下: 

```java
@Override
protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
                throws ServletException, IOException {
    request.setAttribute(HttpServletResponse.class.getName(), response);

    CsrfToken csrfToken = this.tokenRepository.loadToken(request);
    final boolean missingToken = csrfToken == null;
    if (missingToken) {
        csrfToken = this.tokenRepository.generateToken(request);
        this.tokenRepository.saveToken(csrfToken, request, response);
    }
    request.setAttribute(CsrfToken.class.getName(), csrfToken);
    request.setAttribute(csrfToken.getParameterName(), csrfToken);

    if (!this.requireCsrfProtectionMatcher.matches(request)) {
        filterChain.doFilter(request, response);
        return;
    }

    String actualToken = request.getHeader(csrfToken.getHeaderName());
    if (actualToken == null) {
        actualToken = request.getParameter(csrfToken.getParameterName());
    }
    if (!csrfToken.getToken().equals(actualToken)) {
        if (this.logger.isDebugEnabled()) {
            this.logger.debug("Invalid CSRF token found for " + UrlUtils.buildFullRequestUrl(request));
        }
        if (missingToken) {
            this.accessDeniedHandler.handle(request, response, new MissingCsrfTokenException(actualToken));
        }
        else {
            this.accessDeniedHandler.handle(request, response, new InvalidCsrfTokenException(csrfToken, actualToken));
        }
        return;
    }

    filterChain.doFilter(request, response);
}
```

执行记录:

```
ENDPOINT: /login
FIRST-CALL
	|  INFO ... RequestBodyServletRequestWrapFilter : request body servlet request wrap filter ...
	| DEBUG ... RedisCsrfTokenRepository : csrf filter: redis csrf token repository: load token by LoginUser info (LoginUser(username=caplike, password=caplike)).
	| DEBUG ... RedisCsrfTokenRepository : csrf filter: redis csrf token repository: generate token: 53aa5d4b2f7a4574be867dad71a63af3
	| DEBUG ... RedisCsrfTokenRepository : csrf filter: redis csrf token repository: save token
	|  INFO ... SimpleAuthenticationFilter : simple authentication filter: attemptAuthentication ...
SECOND-CALL
	|  INFO ... RequestBodyServletRequestWrapFilter : request body servlet request wrap filter ...
	|  ---> call CsrfFilter 的 doInternalFilter
	| DEBUG ... RedisCsrfTokenRepository : csrf filter: redis csrf token repository: load token by LoginUser info (LoginUser(username=caplike, password=caplike)).
	|  INFO ... SimpleAuthenticationFilter : simple authentication filter: attemptAuthentication ...
```



## 主要涉及的知识点

1. HttpServletRequestWrapper
2. SpringBoot 整合 Redis 用 fastJSON
3. SpringSecurity

# Reference

[CSRF & CORS](https://www.cnblogs.com/lailailai/p/4528092.html)

[SpringBoot + Spring Security 与 CSRF 有关的几个问题](https://blog.51cto.com/360douruanliang/2116591)

# 更新记录

## 2020年4月26日15:25:27

The Very First Version.

## 2020年5月13日09:06:18

新增 CsrfFilter 的描述;

并且更新 token 的缓存方式为单独缓存 (非前一版的设值在一个对象再缓存到 Redis);