package cn.caplike.demo.spring.security.csrf.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * AuthController
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-04-27 08:56
 */
@RestController
public class AuthController {

    @PostMapping("/login")
    public String login() {
        return "login ...";
    }
}
