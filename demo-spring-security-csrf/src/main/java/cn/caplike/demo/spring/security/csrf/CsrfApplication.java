package cn.caplike.demo.spring.security.csrf;

import com.alibaba.fastjson.parser.ParserConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * demo-spring-security-csrf 启动类
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-04-26 11:38
 */
@SpringBootApplication
public class CsrfApplication {

    public static void main(String[] args) {
        SpringApplication.run(CsrfApplication.class);

        ParserConfig.getGlobalInstance().addAccept("cn.caplike.demo.spring.security.csrf.domain.dto.");
    }
}
