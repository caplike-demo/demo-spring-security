package cn.caplike.demo.spring.security.csrf.domain.dto;

import lombok.Data;

/**
 * 登陆用户的 DTO
 *
 * @author LiKe
 * @version 1.0.0
 * @date 2020-04-30 10:00
 */
@Data
public final class LoginUser {

    private String username;

    private String password;

}
